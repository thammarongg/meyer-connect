﻿using Core.Backend.Models.Post;
using Core.Backend.Repositories.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Controllers.Post
{
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/post/v{version:apiVersion}/posts")]
    public class PostsController : Controller
    {
        private readonly ILogger _logger;
        private readonly IPostRepository _repository;
        private readonly IUrlHelper _urlHelper;

        public PostsController(
            ILogger<PostsController> logger,
            IPostRepository repo,
            IUrlHelper urlHelper)
        {
            _logger = logger;
            _repository = repo;
            _urlHelper = urlHelper;
        }

        // GET: api/post/{ApiVersion}/posts
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            dynamic jsonResult = new JObject();

            try
            {
                var items = await _repository.GetAllPostAsync();
                if (!items.Any())
                {
                    _logger.LogInformation("not found any post.");
                    return NotFound();
                }

                jsonResult.items = JToken.FromObject(items, new Newtonsoft.Json.JsonSerializer()
                {
                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                });

                _logger.LogInformation("return post json data.");
                return Ok(jsonResult);
            }
            catch (Exception ex)
            {
                _logger.LogError("err-04-001: something wrong.", new object[] { ex.Message });
                return StatusCode(500, ex.Message);
            }
        }

        // GET: api/post/{ApiVersion}/posts/{id}
        [HttpGet("{id}", Name = "GetPostByPostId")]
        public async Task<IActionResult> GetPostByPostId(int id)
        {
            dynamic jsonResult = new JObject();

            try
            {
                var item = await _repository.GetPostByIdAsync(id);
                if (item == null)
                {
                    _logger.LogInformation("not found any post.");
                    return NotFound();
                }

                jsonResult.item = JToken.FromObject(item);

                _logger.LogInformation("return post json data.");
                return Ok(jsonResult);
            }
            catch (Exception ex)
            {
                _logger.LogError("err-04-002: something wrong.", new object[] { ex.Message });
                return StatusCode(500, ex.Message);
            }
        }

        // GET: api/post/{ApiVersion}/posts/type/{typeId}
        [HttpGet("type/{typeId}", Name = "GetPostByTypeId")]
        public async Task<IActionResult> GetPostByTypeId(int typeId = 0)
        {
            dynamic jsonResult = new JObject();

            try
            {
                var items = await _repository.GetPostPageAsync(1, 5, typeId);
                if (items == null)
                {
                    _logger.LogInformation("not found any post");
                    return NotFound();
                }

                jsonResult.items = JToken.FromObject(items);

                _logger.LogInformation("return post json data.");
                return Ok(jsonResult);
            }
            catch (Exception ex)
            {
                _logger.LogError("err-04-002: something wrong.", new object[] { ex.Message });
                return StatusCode(500, ex.Message);
            }
        }

        // GET: api/post/{ApiVersion}/posts/type/{typeId}/{statusId}
        [Authorize]
        [HttpGet("type/{typeId}/{statusId}", Name = "GetPostByTypeIdandStatusId")]
        public async Task<IActionResult> GetPostByTypeIdandStatusId(int typeId = 0, int statusId = 1)
        {
            dynamic jsonResult = new JObject();

            try
            {
                var items = await _repository.GetPostPageAsync(1, 5, typeId, statusId);
                if (items == null)
                {
                    _logger.LogInformation("not found any post");
                    return NotFound();
                }

                jsonResult.items = JToken.FromObject(items);

                _logger.LogInformation("return post json data.");
                return Ok(jsonResult);
            }
            catch (Exception ex)
            {
                _logger.LogError("err-04-002: something wrong.", new object[] { ex.Message });
                return StatusCode(500, ex.Message);
            }
        }

        // POST: api/post/{ApiVersion}/posts
        [HttpPost]
        public async Task<IActionResult> Create([FromBody]Posts item)
        {
            dynamic jsonResult = new JObject();

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                item.Author = "Administrator";
                item.CreatedPostBy = "Administrator";
                item.LastModifiedBy = "Administrator";
                item.Revise = 0;

                if (await _repository.CreatePostAsync(item))
                {
                    _logger.LogInformation("created post.");
                    return StatusCode(201);
                }
                else
                {
                    _logger.LogWarning("can not create post.");
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("err-04-003: something wrong.", new object[] { ex.Message });
                return StatusCode(500, ex.Message);
            }
        }

        //// PUT: api/post/{ApiVersion}/posts/{id}
        //[HttpPut("{id}")]
        //public async Task<IActionResult> Update(int id, [FromBody]Posts item)
        //{
        //    dynamic jsonResult = new JObject();

        //    if (!ModelState.IsValid || item.PostId != id)
        //    {
        //        return BadRequest();
        //    }

        //    try
        //    {
        //        var post = await _repository.GetPostByIdAsync(id);
        //        if (post == null)
        //        {
        //            _logger.LogInformation("not found any post.");
        //            return NotFound();
        //        }

        //        //

        //        if (await _repository.UpdatePostAsync(post))
        //        {
        //            _logger.LogInformation("updated post.");
        //            return new NoContentResult();
        //        }
        //        else
        //        {
        //            _logger.LogWarning("can not update post.");
        //            return BadRequest();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError("err-04-004: something wrong.", new object[] { ex.Message });
        //        return StatusCode(500, ex.Message);
        //    }
        //}

        // PUT: api/post/{ApiVersion}/posts
        [HttpPut]
        public async Task<IActionResult> Update([FromBody]Posts item)
        {
            dynamic jsonResult = new JObject();

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                item.Author = "Administrator";
                item.CreatedPostBy = "Administrator";
                item.LastModifiedBy = "Administrator";
                item.Revise = 0;

                var post = await _repository.GetPostByIdAsync(item.PostId);
                if (post == null)
                {
                    _logger.LogInformation("not found any post.");
                    return NotFound();
                }

                post.TitleName = item.TitleName;
                post.Contents = item.Contents;
                post.Author = item.Author;
                post.Revise = item.Revise;
                post.PostStatus = item.PostStatus;
                post.LastModifiedDateUtc = DateTime.UtcNow;
                post.CreatedPostBy = item.CreatedPostBy;
                post.LastModifiedBy = item.LastModifiedBy;

                //

                if (await _repository.UpdatePostAsync(post))
                {
                    _logger.LogInformation("updated post.");
                    return new NoContentResult();
                }
                else
                {
                    _logger.LogWarning("can not update post.");
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("err-04-004: something wrong.", new object[] { ex.Message });
                return StatusCode(500, ex.Message);
            }
        }

        // DELETE: api/post/{ApiVersion}/posts/{id}
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            dynamic jsonResult = new JObject();

            try
            {
                var post = await _repository.GetPostByIdAsync(id);
                if (post == null)
                {
                    _logger.LogInformation("not found any post.");
                    return NotFound();
                }

                if (await _repository.DeletePostAsync(post))
                {
                    _logger.LogInformation("deleted post.");
                    return new NoContentResult();
                }
                else
                {
                    _logger.LogWarning("can not delete post.");
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("err-04-005: something wrong.", new object[] { ex.Message });
                return StatusCode(500, ex.Message);
            }
        }
    }
}