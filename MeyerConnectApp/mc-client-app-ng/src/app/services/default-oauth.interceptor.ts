import { Injectable, Inject, Optional } from '@angular/core';
import {
  OAuthService,
  OAuthStorage,
  OAuthModuleConfig,
  OAuthResourceServerErrorHandler
} from 'angular-oauth2-oidc';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

@Injectable()
export class DefaultOAuthInterceptor implements HttpInterceptor {
  constructor(
    private oauthStorage: OAuthStorage,
    private oauthServer: OAuthService,
    private errorHandler: OAuthResourceServerErrorHandler,
    @Optional() private oauthModuleConfig: OAuthModuleConfig
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const sendAccessToken = this.oauthModuleConfig.resourceServer
      .sendAccessToken;

    if (sendAccessToken) {
      const token = this.oauthStorage.getItem('access_token');
      req = req.clone({
        setHeaders: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
          Authorization: `Bearer ${token}`
        }
      });
    }
    return next
      .handle(req)
      .pipe(catchError(err => this.errorHandler.handleError(err)));
  }
}
