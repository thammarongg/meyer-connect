import { StaffPhone } from './staff-phone';

export class Staff {
    EmployeeId: string;
    JobTitle: string;
    Email: string;
    FullName: string;
    Phones: StaffPhone[];
    Division: string;
    CompanyName: string;
    Department: string;
    City: string;
    AccountProfileImageUrl: string;
}
