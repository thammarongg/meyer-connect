"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _semanticUiReact = require("semantic-ui-react");

var _link = require("next\\dist\\lib\\link.js");

var _link2 = _interopRequireDefault(_link);

var _head = require("next\\dist\\lib\\head.js");

var _head2 = _interopRequireDefault(_head);

var _profileMenu = require("../components/profile/profileMenu");

var _profileMenu2 = _interopRequireDefault(_profileMenu);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = "C:\\Projects\\BitBucket\\meyer-connect\\MeyerConnectApp\\client-app-nextjs\\components\\layout.js";


var logoImage = "/static/images/mc-logo.png";

exports.default = function (_ref) {
  var children = _ref.children,
      _ref$title = _ref.title,
      title = _ref$title === undefined ? "default title" : _ref$title;
  return _react2.default.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    }
  }, _react2.default.createElement(_head2.default, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    }
  }, _react2.default.createElement("title", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    }
  }, title), _react2.default.createElement("meta", { charSet: "utf-8", __source: {
      fileName: _jsxFileName,
      lineNumber: 24
    }
  }), _react2.default.createElement("meta", { name: "viewport", content: "initial-scale=1.0, width=device-width", __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    }
  }), _react2.default.createElement("link", {
    rel: "stylesheet",
    href: "//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/semantic.min.css",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26
    }
  })), _react2.default.createElement("header", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    }
  }, _react2.default.createElement(_semanticUiReact.Menu, { color: "blue", inverted: true, borderless: true, fixed: "top", __source: {
      fileName: _jsxFileName,
      lineNumber: 32
    }
  }, _react2.default.createElement(_semanticUiReact.Menu.Item, { as: "a", name: "home", __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    }
  }), _react2.default.createElement(_semanticUiReact.Menu.Item, { as: "a", name: "fuck", __source: {
      fileName: _jsxFileName,
      lineNumber: 34
    }
  }), _react2.default.createElement(_semanticUiReact.Menu.Item, { as: "a", className: "logo", __source: {
      fileName: _jsxFileName,
      lineNumber: 35
    }
  }, _react2.default.createElement(_semanticUiReact.Image, { src: logoImage, size: "small", __source: {
      fileName: _jsxFileName,
      lineNumber: 36
    }
  })), _react2.default.createElement(_semanticUiReact.Menu.Menu, { position: "right", __source: {
      fileName: _jsxFileName,
      lineNumber: 38
    }
  }, _react2.default.createElement(_semanticUiReact.Menu.Item, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39
    }
  }, _react2.default.createElement(_profileMenu2.default, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40
    }
  }))))), children);
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHNcXGxheW91dC5qcyJdLCJuYW1lcyI6WyJEcm9wZG93biIsIklucHV0IiwiTWVudSIsIkNvbnRhaW5lciIsIkdyaWQiLCJIZWFkZXIiLCJJbWFnZSIsIkxpc3QiLCJTZWdtZW50IiwiRGl2aWRlciIsIkxpbmsiLCJIZWFkIiwiUHJvZmlsZU1lbnUiLCJsb2dvSW1hZ2UiLCJjaGlsZHJlbiIsInRpdGxlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEsQUFDRSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFDQSxBQUNBLEFBQ0EsQUFDQTs7QUFFRixBQUFPOzs7O0FBQ1AsQUFBTzs7OztBQUVQLEFBQU8sQUFBaUI7Ozs7Ozs7OztBQUV4QixJQUFNLFlBQU4sQUFBa0IsQUFFbEI7O2tCQUFlLGdCQUFBO01BQUEsQUFBRyxnQkFBSCxBQUFHO3dCQUFILEFBQWE7TUFBYixBQUFhLG1DQUFiLEFBQXFCLGtCQUFyQjt5QkFDYixjQUFBOztnQkFBQTtrQkFBQSxBQUNFO0FBREY7QUFBQSxHQUFBLGtCQUNFLEFBQUM7O2dCQUFEO2tCQUFBLEFBQ0U7QUFERjtBQUFBLHFCQUNFLGNBQUE7O2dCQUFBO2tCQUFBLEFBQVE7QUFBUjtBQUFBLEtBREYsQUFDRSxBQUNBLGdEQUFNLFNBQU4sQUFBYztnQkFBZDtrQkFGRixBQUVFLEFBQ0E7QUFEQTs4Q0FDTSxNQUFOLEFBQVcsWUFBVyxTQUF0QixBQUE4QjtnQkFBOUI7a0JBSEYsQUFHRSxBQUNBO0FBREE7O1NBQ0EsQUFDTSxBQUNKO1VBRkYsQUFFTzs7Z0JBRlA7a0JBTEosQUFDRSxBQUlFLEFBS0Y7QUFMRTtBQUNFLHVCQUlKLGNBQUE7O2dCQUFBO2tCQUFBLEFBQ0U7QUFERjtBQUFBLHFCQUNFLEFBQUMsdUNBQUssT0FBTixBQUFZLFFBQU8sVUFBbkIsTUFBNEIsWUFBNUIsTUFBdUMsT0FBdkMsQUFBNkM7Z0JBQTdDO2tCQUFBLEFBQ0U7QUFERjttQ0FDRSxBQUFDLHNCQUFELEFBQU0sUUFBSyxJQUFYLEFBQWMsS0FBSSxNQUFsQixBQUF1QjtnQkFBdkI7a0JBREYsQUFDRSxBQUNBO0FBREE7b0NBQ0EsQUFBQyxzQkFBRCxBQUFNLFFBQUssSUFBWCxBQUFjLEtBQUksTUFBbEIsQUFBdUI7Z0JBQXZCO2tCQUZGLEFBRUUsQUFDQTtBQURBO3NCQUNDLGNBQUQsc0JBQUEsQUFBTSxRQUFLLElBQVgsQUFBYyxLQUFJLFdBQWxCLEFBQTRCO2dCQUE1QjtrQkFBQSxBQUNFO0FBREY7cUJBQ0UsQUFBQyx3Q0FBTSxLQUFQLEFBQVksV0FBVyxNQUF2QixBQUE0QjtnQkFBNUI7a0JBSkosQUFHRSxBQUNFLEFBRUY7QUFGRTt1QkFFRCxjQUFELHNCQUFBLEFBQU0sUUFBSyxVQUFYLEFBQW9CO2dCQUFwQjtrQkFBQSxBQUNFO0FBREY7cUJBQ0csY0FBRCxzQkFBQSxBQUFNOztnQkFBTjtrQkFBQSxBQUNFO0FBREY7QUFBQSxxQkFDRSxBQUFDOztnQkFBRDtrQkFuQlYsQUFVRSxBQUNFLEFBTUUsQUFDRSxBQUNFLEFBTVA7QUFOTztBQUFBLFVBcEJHLEFBQ2I7QUFERiIsImZpbGUiOiJsYXlvdXQuanMiLCJzb3VyY2VSb290IjoiQzovUHJvamVjdHMvQml0QnVja2V0L21leWVyLWNvbm5lY3QvTWV5ZXJDb25uZWN0QXBwL2NsaWVudC1hcHAtbmV4dGpzIn0=