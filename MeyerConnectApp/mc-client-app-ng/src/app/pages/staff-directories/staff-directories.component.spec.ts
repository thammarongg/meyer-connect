import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffDirectoriesComponent } from './staff-directories.component';

describe('StaffDirectoriesComponent', () => {
  let component: StaffDirectoriesComponent;
  let fixture: ComponentFixture<StaffDirectoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffDirectoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffDirectoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
