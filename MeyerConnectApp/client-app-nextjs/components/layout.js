import {
  Dropdown,
  Input,
  Menu,
  Container,
  Grid,
  Header,
  Image,
  List,
  Segment,
  Divider
} from "semantic-ui-react"
import Link from "next/link"
import Head from "next/head"

import ProfileMenu from "../components/profile/profileMenu"

const logoImage = "/static/images/mc-logo.png"

export default ({ children, title = "default title" }) => (
  <div>
    <Head>
      <title>{title}</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <link
        rel="stylesheet"
        href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/semantic.min.css"
      />
    </Head>
    <header>
      <Menu color="blue" inverted borderless fixed="top">
        <Menu.Item as="a" name="home" />
        <Menu.Item as="a" name="fuck" />
        <Menu.Item as="a" className="logo">
          <Image src={logoImage} size="small" />
        </Menu.Item>
        <Menu.Menu position="right">
          <Menu.Item>
            <ProfileMenu />
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    </header>

    {children}
  </div>
)
