﻿using Core.Backend.Models;
using Core.Backend.Models.Storage;
using Core.Backend.Repositories.Interface;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Repositories
{
    public class StorageRepository : IStorageRepository
    {
        private readonly AppDbContext _context;

        public StorageRepository(
            AppDbContext context)
        {
            _context = context;
        }

        public async Task<bool> CreateFileDataAsync(Storages storages)
        {
            _context.Storages.Add(storages);

            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<bool> DeleteFileDataAsync(Storages storages)
        {
            _context.Storages.Remove(storages);

            return await _context.SaveChangesAsync() > 0;
        }

        public List<Storages> GetAllFile()
        {
            return _context.Storages.ToList();
        }

        public Storages GetFileById(string fileId)
        {
            return _context.Storages
                .Where(c => c.FileId == fileId)
                .FirstOrDefault();
        }

        public async Task<bool> UpdateFileDataAsync(Storages storages)
        {
            _context.Storages.Update(storages);

            return await _context.SaveChangesAsync() > 0;
        }
    }
}