﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Core.Backend.Data.Migrations.MeyerConnectAppDb
{
    public partial class UpdatePostsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Team",
                table: "PostAudiences",
                newName: "StaffTeam");

            migrationBuilder.RenameColumn(
                name: "Level",
                table: "PostAudiences",
                newName: "StaffLevel");

            migrationBuilder.RenameColumn(
                name: "Group",
                table: "PostAudiences",
                newName: "StaffGroup");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "StaffTeam",
                table: "PostAudiences",
                newName: "Team");

            migrationBuilder.RenameColumn(
                name: "StaffLevel",
                table: "PostAudiences",
                newName: "Level");

            migrationBuilder.RenameColumn(
                name: "StaffGroup",
                table: "PostAudiences",
                newName: "Group");
        }
    }
}
