﻿using Core.Secure.Models;
using IdentityModel;
using IdentityServer4;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Core.Secure.Services
{
    public class ClaimsProfileService : IProfileService
    {
        private readonly IUserClaimsPrincipalFactory<Accounts> _claimsFactory;
        private readonly UserManager<Accounts> _userManager;

        public ClaimsProfileService(
            UserManager<Accounts> userManager,
            IUserClaimsPrincipalFactory<Accounts> claimsFactory)
        {
            _userManager = userManager;
            _claimsFactory = claimsFactory;
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var sub = context.Subject.GetSubjectId();
            if (sub == null) throw new ArgumentNullException(nameof(context.Subject));
            var user = await _userManager.FindByIdAsync(sub);
            if (user == null) throw new ArgumentException("Invalid subject identifier");

            var principal = await _claimsFactory.CreateAsync(user);

            var claims = principal.Claims.ToList();
            claims = claims.Where(claim => context.RequestedClaimTypes.Contains(claim.Type)).ToList();
            claims.Add(new Claim(JwtClaimTypes.PreferredUserName, user.UserName));
            claims.Add(new Claim(JwtClaimTypes.Picture, user.AccountProfileImageUrl));

            if (user.DefaultRole == "sysadmin")
            {
                claims.Add(new Claim(JwtClaimTypes.Role, "sysadmin"));
            }
            else if (user.DefaultRole == "moderator")
            {
                claims.Add(new Claim(JwtClaimTypes.Role, "moderator"));
            }
            else
            {
                claims.Add(new Claim(JwtClaimTypes.Role, "staff"));
            }

            claims.Add(new Claim(IdentityServerConstants.StandardScopes.Email, user.Email));
            context.IssuedClaims = claims;
        }

        //public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        //{
        //    var sub = context.Subject.GetSubjectId();
        //    if (sub == null) throw new ArgumentNullException(nameof(context.Subject));

        //    var user = await _userManager.FindByIdAsync(sub);
        //    if (user == null) throw new ArgumentException("Invalid subject identifier");

        //    var principal = await _claimsFactory.CreateAsync(user);
        //    var claims = principal.Claims.ToList();

        //    claims = claims.Where(claim => context.RequestedClaimTypes.Contains(claim.Type)).ToList();
        //    claims.Add(new Claim(JwtClaimTypes.Subject, user.Id));
        //    claims.Add(new Claim(JwtClaimTypes.PreferredUserName, user.UserName));
        //    claims.Add(new Claim(JwtClaimTypes.Role, user.PermissionRole));
        //    claims.Add(new Claim(JwtClaimTypes.Role, "mcPermissionRoles"));
        //    claims.Add(new Claim(JwtClaimTypes.Scope, "mcPermissionRoles"));
        //    claims.Add(new Claim(JwtClaimTypes.Picture, user.AccountProfileImageUrl));
        //    claims.Add(new Claim(IdentityServerConstants.StandardScopes.Email, user.Email));

        //    context.IssuedClaims = claims;
        //}

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var sub = context.Subject.GetSubjectId();
            var user = await _userManager.FindByIdAsync(sub);
            context.IsActive = user != null;
        }
    }
}