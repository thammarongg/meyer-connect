import { StaffService } from './staff.service';
// import { Injectable, EventEmitter } from '@angular/core';
// import {
//   UserManager,
//   UserManagerSettings,
//   User,
//   WebStorageStateStore
// } from 'oidc-client';
// import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Observable } from 'rxjs/Observable';
// import { of } from 'rxjs/observable/of';
// import { catchError, map, tap } from 'rxjs/operators';

// export function getClientSetting(): UserManagerSettings {
//   return {
//     authority: 'http://localhost:60000',
//     client_id: '8611c27e76b358c742a4ff60ac909d07@meyer-connect.com',
//     redirect_uri: `${window.location.origin}/auth-callback`,
//     post_logout_redirect_uri: `${window.location.origin}/`,
//     response_type: 'id_token token',
//     scope: 'openid profile mcPermissionRoles',
//     filterProtocolClaims: true,
//     loadUserInfo: true,
//     userStore: new WebStorageStateStore({ store: window.localStorage }),
//     automaticSilentRenew: true,
//     silent_redirect_uri: `${window.location.origin}/slient-token-refresh.html`
//   };
// }
// @Injectable()
// export class AuthService {
//   private manager: UserManager = new UserManager(getClientSetting());
//   userLoadedEvent: EventEmitter<User> = new EventEmitter<User>();
//   private user: User = null;

//   authHeaders: Headers;

//   constructor(private http: HttpClient) {
//     this.manager.getUser().then(user => {
//       if (user) {
//         this.user = user;
//         this.userLoadedEvent.emit(user);
//       }
//     });
//   }

//   isLoggedIn(): boolean {
//     return this.user != null && !this.user.expired;
//   }

//   loggedOut(): Promise<void> {
//     return this.manager.signoutRedirect();
//   }

//   getClaims(): any {
//     return this.user.profile;
//   }

//   getAuthorizationHeaderValue(): string {
//     return `${this.user.token_type} ${this.user.access_token}`;
//   }

//   startAuthentication(): Promise<void> {
//     return this.manager.signinRedirect();
//   }

//   completeAuthentication(): Promise<void> {
//     return this.manager.signinRedirectCallback().then(user => {
//       this.user = user;
//     });
//   }
// }

import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';

import { Observable } from 'rxjs';




@Injectable()
export class AuthService {
  userProfile: object;
  constructor(
    private oauthService: OAuthService,
    private staffService: StaffService
  ) {}

  login() {
    this.oauthService.initImplicitFlow();
  }

  logout() {
    localStorage.removeItem('mc.profileImage');
    this.oauthService.logOut();
  }

  startAuthorization() {
    this.oauthService.initImplicitFlow();
  }

  get isAdminAccount(): boolean {
    this.staffService.getIsAdminAccount().subscribe((res: boolean) => {
      console.log('res:'+res);
      return true;
    });

    return false;
  }

  loadUserProfile(): object {
    this.oauthService.loadUserProfile().then(data => {
      this.userProfile = data;
    });
    return this.userProfile;
  }

  getClaims(): any {
    const claims: any = this.oauthService.getIdentityClaims();
    if (!claims) {
      return null;
    }
    return claims;
  }

  set requestAccessToken(value: boolean) {
    this.oauthService.requestAccessToken = value;
    localStorage.setItem('requestAccessToken', '' + value);
  }

  get requestAccessToken() {
    return this.oauthService.requestAccessToken;
  }

  get id_token() {
    return this.oauthService.getIdToken();
  }

  get access_token() {
    return this.oauthService.getAccessToken();
  }

  get id_token_expiration() {
    return this.oauthService.getIdTokenExpiration();
  }

  get access_token_expiration() {
    return this.oauthService.getAccessTokenExpiration();
  }
}
