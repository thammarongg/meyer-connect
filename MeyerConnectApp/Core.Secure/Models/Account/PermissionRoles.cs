﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Secure.Models.Account
{
    public class PermissionRoles
    {
        [Key]
        public int Seq { get; set; }

        public string Name { get; set; }

    }
}
