﻿using Core.Backend.Repositories;
using Core.Backend.Repositories.Interface;
using Core.Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Core.Backend.Controllers.Wiki
{
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/Wiki/Tracker")]
    public class TrackerController : Controller
    {
        private readonly ILogger _logger;
        private readonly IEmailSender _emailSerder;
        private readonly IUrlHelper _urlHelper;
        private readonly IGoogleAnalyticService _googleAnalyticService;
        private readonly IStaffsRepository _staffsRepository;

        public TrackerController(
            ILogger<TrackerController> logger,
            IEmailSender emailSender,
            IUrlHelper urlHelper,
            IGoogleAnalyticService googleAnalyticService,
            IStaffsRepository staffsRepository)
        {
            _googleAnalyticService = googleAnalyticService;
            _logger = logger;
            _emailSerder = emailSender;
            _urlHelper = urlHelper;
            _staffsRepository = staffsRepository;
        }

        // GET: api/{ApiVersion}/Tracker
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GetAsync()
        {
            try
            {
                var client = new HttpClient();

                var content = new FormUrlEncodedContent(new[]
                {
                   new KeyValuePair<string,string>("username","apiaccess"),
                   new KeyValuePair<string,string>("password","apiAccess@Meyer")
                });

                var tokenResult = await client.PostAsync("https://wiki.meyer-connect.com/wp-json/jwt-auth/v1/token", content);
                var tokenData = JsonConvert.DeserializeObject<WikiJwtInfoViewModel>(await tokenResult.Content.ReadAsStringAsync());

                client.SetBearerToken(tokenData.token);
                var userWikiResult = await client.GetAsync("https://wiki.meyer-connect.com/wp-json/wp/v2/users?per_page=100&context=edit");
                var userWikiData = JsonConvert.DeserializeObject<List<WikiUserInfoViewModel>>(await userWikiResult.Content.ReadAsStringAsync());

                client.Dispose();

                // mapping
                foreach (var userWiki in userWikiData)
                {
                    var account = await _staffsRepository.GetByEmail(userWiki.email);
                    if (account != null)
                    {
                    }
                }

                return Ok(_googleAnalyticService.GetAnalyticData());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("{userId}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetAnalyticDataByUser(string userId)
        {
            try
            {
                var account = await _staffsRepository.GetById(userId);
                if (account != null)
                {
                    var client = new HttpClient();

                    var content = new FormUrlEncodedContent(new[]
                    {
                   new KeyValuePair<string,string>("username","apiaccess"),
                   new KeyValuePair<string,string>("password","apiAccess@Meyer")
                    });

                    var tokenResult = await client.PostAsync("https://wiki.meyer-connect.com/wp-json/jwt-auth/v1/token", content);
                    var tokenData = JsonConvert.DeserializeObject<WikiJwtInfoViewModel>(await tokenResult.Content.ReadAsStringAsync());

                    client.SetBearerToken(tokenData.token);
                    var userWikiResult = await client.GetAsync("https://wiki.meyer-connect.com/wp-json/wp/v2/users?per_page=100&context=edit&search=" + account.Email);
                    var userWikiData = JsonConvert.DeserializeObject<List<WikiUserInfoViewModel>>(await userWikiResult.Content.ReadAsStringAsync());

                    client.Dispose();

                    if (!userWikiData.Any())
                    {
                        return NotFound("no user data");
                    }

                    var userWiki = userWikiData.FirstOrDefault();
                    var analyticData = _googleAnalyticService.GetAnalyticDataByUser(userWiki.id);

                    return Ok(analyticData);
                }
                else
                {
                    return NotFound("no analytic data");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        } 

        // GET: api/Tracking/5

        // POST: api/Tracking
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Tracking/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    public class ResultContainer<T>
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public T Result { get; set; }
    }

    public class WikiJwtInfoViewModel
    {
        public string token { get; set; }
        public string user_email { get; set; }
        public string user_nicename { get; set; }
        public string user_display_name { get; set; }
    }

    public class WikiUserInfoViewModel
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("username")]
        public string username { get; set; }

        [JsonProperty("name")]
        public string name { get; set; }

        [JsonProperty("first_name")]
        public string first_name { get; set; }

        [JsonProperty("last_name")]
        public string last_name { get; set; }

        [JsonProperty("email")]
        public string email { get; set; }
    }
}