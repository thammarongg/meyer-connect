﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Secure.Data.Migrations.MeyerConnectAppDb
{
    public partial class ChangeAccountsColumnName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PermissionRole",
                table: "Accounts",
                newName: "DefaultRole");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DefaultRole",
                table: "Accounts",
                newName: "PermissionRole");
        }
    }
}
