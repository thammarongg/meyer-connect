﻿using Core.Backend.Models;
using Core.Backend.Models.Staff;
using Core.Backend.Models.Utils;
using Core.Backend.Repositories;
using Core.Backend.Repositories.Interface;
using Core.Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Controllers.Staff
{
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/staff/v{version:apiVersion}/StaffList")]
    //[Authorize]
    public class StaffListController : Controller
    {
        private readonly AppDbContext _context;
        private readonly ILogger _logger;
        private readonly IEmailSender _emailSender;
        private readonly IStaffsRepository _repo;
        private readonly IUrlHelper _urlHelper;

        public StaffListController(
            AppDbContext context,
            ILogger<StaffListController> logger,
            IEmailSender emailSender,
            IStaffsRepository repo,
            IUrlHelper urlHelper)
        {
            _context = context;
            _logger = logger;
            _emailSender = emailSender;
            _repo = repo;
            _urlHelper = urlHelper;
        }

        // GET: api/Staff/{ApiVersion}//StaffList
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            dynamic jsonResult = new JObject();

            try
            {
                var items = await _repo.GetAll();
                if (items == null)
                {
                    _logger.LogInformation("not found any data.");
                    return NotFound();
                }

                jsonResult.items = JToken.FromObject(items, new Newtonsoft.Json.JsonSerializer() {
                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                });

                _logger.LogInformation("return json data");
                return Ok(jsonResult);
            }
            catch (Exception ex)
            {
                _logger.LogError("err-01-001: something wrong.", new object[] { ex.Message });
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        [Route("find")]
        public async Task<IActionResult> GetByColleague([FromQuery]StaffQueryModel model)
        {
            dynamic jsonResult = new JObject();

            try
            {
                var items = await _repo.GetByColleague(model);
                jsonResult.items = JToken.FromObject(items, new Newtonsoft.Json.JsonSerializer()
                {
                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                });

                _logger.LogInformation("return json data");
                return Ok(jsonResult);
            }
            catch (Exception ex)
            {
                _logger.LogError("err-01-001: something wrong.", new object[] { ex.Message });
                jsonResult.message = ex.Message;
                return StatusCode(500, jsonResult);
            }
        }

        [HttpGet]
        [Route("filter")]
        public async Task<IActionResult> GetByFilter([FromQuery] StaffQueryModel model)
        {
            dynamic jsonResult = new JObject();

            try
            {
                var items = await _repo.GetByFilter(model);
                jsonResult.items = JToken.FromObject(items);

                _logger.LogInformation("return json data");
                return Ok(jsonResult);
            }
            catch (Exception ex)
            {
                _logger.LogError("err-01-001: something wrong.", new object[] { ex.Message });
                jsonResult.message = ex.Message;
                return StatusCode(500, jsonResult);
            }
        }

        [HttpGet]
        [Route("initial")]
        public async Task<IActionResult> GetInitialFilter()
        {
            dynamic jsonResult = new JObject();

            try
            {
                var item = await _repo.GetInitialFilter();
                if (item == null)
                {
                    _logger.LogWarning("warn-01-004: not found data.");
                    return NotFound("not found data.");
                }

                _logger.LogInformation("return json data");

                return Ok(item);
            }
            catch (Exception ex)
            {
                _logger.LogError("err-01-004: something wrong.", new object[] { ex.Message });
                jsonResult.message = ex.Message;
                return StatusCode(500, jsonResult);
            }
        }

        // GET: api/{ApiVersion}/Staff/StaffList/Pageing
        //[HttpGet(Name = "GetStaffListWithPagination")]
        //public IActionResult Get(PagingParams pagingParams)
        //{
        //    dynamic jsonResult = new JObject();

        //    try
        //    {
        //        var model = _repo.GetAll(pagingParams);
        //        Response.Headers.Add("X-Pagination", model.GetHeader().ToJson());

        //        jsonResult.paging = JToken.FromObject(model.GetHeader());
        //        jsonResult.links = JToken.FromObject(GetLinks(model));
        //        jsonResult.items = JToken.FromObject(model.List.ToList());

        //        return Ok(jsonResult);

        //    } catch (Exception ex)
        //    {
        //        _logger.LogError("err-01-001: something wrong.", new object[] { ex.Message });
        //        jsonResult.message = ex.Message;
        //        return StatusCode(500, jsonResult);
        //    }
        //}

        #region Helper

        private List<LinkInfo> GetLinks(PagedList<Accounts> list)
        {
            var links = new List<LinkInfo>();

            if (list.HasPreviousPage)
                links.Add(CreateLink("GetStaffListWithPagination", list.PreviousPageNumber,
                           list.PageSize, "previousPage", "GET"));

            links.Add(CreateLink("GetStaffListWithPagination", list.PageNumber,
                           list.PageSize, "self", "GET"));

            if (list.HasNextPage)
                links.Add(CreateLink("GetStaffListWithPagination", list.NextPageNumber,
                           list.PageSize, "nextPage", "GET"));

            return links;
        }

        private LinkInfo CreateLink(
            string routeName, int pageNumber, int pageSize,
            string rel, string method)
        {
            return new LinkInfo
            {
                Href = _urlHelper.Link(routeName,
                            new { PageNumber = pageNumber, PageSize = pageSize }),
                Rel = rel,
                Method = method
            };
        }

        #endregion Helper
    }
}