import { Component, OnInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-page-layout',
  templateUrl: './page-layout.component.html',
  styleUrls: ['./page-layout.component.css']
})
export class PageLayoutComponent implements OnInit {
  profileImage = localStorage.getItem('mc.profileImage')
    ? localStorage.getItem('mc.profileImage')
    : 'assets/images/empty-profile-image.png';
  constructor(private oauthService: OAuthService) {}

  ngOnInit() {
    this.oauthService
      .loadUserProfile()
      .then(userProfile => {
        if (userProfile) {
          localStorage.setItem('mc.profileImage', userProfile['picture']);
          this.profileImage = userProfile['picture'];
        }
      })
      .catch(console.log);
  }
}
