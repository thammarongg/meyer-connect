import { AdminAuthGuardService } from './services/admin-auth-guard.service';
import { NewsAnnouncementComponent } from './pages/news-announcement/news-announcement.component';
import { AdminModule } from './admin/admin.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { TruncateModule } from 'ng2-truncate';
import { TimeAgoPipe } from './shared/time-ago.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Router } from '@angular/router';
import {
  OAuthService,
  UrlHelperService,
  AuthConfig,
  JwksValidationHandler,
  OAuthModule,
  ValidationHandler
} from 'angular-oauth2-oidc';
import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './shared/header/header.component';
import { WeatherComponent } from './shared/weather/weather.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { WeatherService } from './shared/weather/weather.service';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { AuthGuardService } from './services/auth-guard.service';
import { AuthService } from './services/auth.service';
import { RequestAuthComponent } from './request-auth/request-auth.component';
import { AuthCallbackComponent } from './auth-callback/auth-callback.component';
import { SharedModule } from './shared/shared.module';
import { HelpComponent } from './pages/help/help.component';
import { PageLayoutComponent } from './pages/page-layout/page-layout.component';
import { StaffDirectoriesComponent } from './pages/staff-directories/staff-directories.component';
import { NewsAnnouncementService } from './services/news-announcement.service';
import { StaffService } from './services/staff.service';
import { CalendarComponent } from './pages/calendar/calendar.component';
import { SortService } from './services/sort.service';

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    AppRoutingModule,
    AdminModule,
    TruncateModule,
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: [environment.apiUrl],
        sendAccessToken: true
      }
    }),
    SharedModule,
    BrowserAnimationsModule
  ],
  declarations: [
    AppComponent,
    WeatherComponent,
    NotfoundComponent,
    SidebarComponent,
    RequestAuthComponent,
    AuthCallbackComponent,
    HelpComponent,
    // HeaderComponent,
    PageLayoutComponent,
    NewsAnnouncementComponent,
    StaffDirectoriesComponent,
    CalendarComponent
  ],
  providers: [
    WeatherService,
    OAuthService,
    UrlHelperService,
    AuthGuardService,
    AuthService,
    NewsAnnouncementService,
    StaffService,
    AdminAuthGuardService,
    SortService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  // Diagnostic only: inspect router configuration
  constructor(router: Router) {
    console.log('Routes: ', JSON.stringify(router.config, undefined, 2));
  }
}
