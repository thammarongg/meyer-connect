﻿using Core.Secure.Models.Account;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Core.Secure.Models
{
    public class AuthDbContext : IdentityDbContext<Accounts>
    {
        public AuthDbContext(DbContextOptions<AuthDbContext> options)
            : base(options)
        {
        }

        public DbSet<Accounts> Accounts { get; set; }
        public DbSet<AccountProfile> AccountProfiles { get; set; }
        public DbSet<AccountPhoneContact> AccountPhoneContacts { get; set; }
        public DbSet<AccountTeammate> AccountTeammates { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Accounts>()
                .ToTable("Accounts")
                .HasMany(p => p.AccountPhoneContacts)
                .WithOne(p => p.Accounts)
                .HasForeignKey(k => k.UserId)
                .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Accounts>()
                .ToTable("Accounts")
                .HasOne(p => p.AccountProfile)
                .WithOne(p => p.Accounts)
                .HasForeignKey<AccountProfile>(b=>b.UserId)
                .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Accounts>()
                .ToTable("Accounts")
                .HasMany(p => p.AccountTeammates)
                .WithOne(p => p.Accounts)
                .HasForeignKey(k => k.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            // -- set default value -- //
            builder.Entity<Accounts>()
                .Property(b => b.CreatedAccountDateUtc)
                .HasDefaultValueSql("GETUTCDATE()");

            builder.Entity<AccountProfile>().ToTable("AccountProfile");
            builder.Entity<AccountPhoneContact>().ToTable("AccountPhoneContact");
            builder.Entity<IdentityRole>().ToTable("Roles");
            builder.Entity<IdentityUserRole<string>>().ToTable("AccountRoles");
            builder.Entity<IdentityUserClaim<string>>().ToTable("AccountClaims");
            builder.Entity<IdentityUserLogin<string>>().ToTable("AccountLogins");
            builder.Entity<IdentityRoleClaim<string>>().ToTable("AccountRoleClaims");
            builder.Entity<IdentityUserToken<string>>().ToTable("AccountTokens");
        }
    }
}