﻿using Core.Backend.Models;
using Core.Backend.Models.Ltes;
using Core.Backend.Models.Staff;
using Core.Backend.Models.Utils;
using Core.Backend.Repositories.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        private readonly AppDbContext _context;

        public AccountRepository(
            AppDbContext context
            )
        {
            _context = context;
        }

        public IEnumerable<AccountHierarchyModel> FindAccountHierarchy(IEnumerable<Accounts> allAccount, Accounts parentAccount)
        {
            string parentUserId = null;
            string parentEmail = null;

            if (parentAccount != null)
            {
                parentUserId = parentAccount.Id;
                parentEmail = parentAccount.Email;
            }

            var childAccount = allAccount.Where(e => e.AccountProfile.ManagerEmail == parentEmail);

            Collection<AccountHierarchyModel> hierarchy = new Collection<AccountHierarchyModel>();

            foreach (var account in childAccount)

                hierarchy.Add(new AccountHierarchyModel()
                {
                    accounts = account,
                    accountHierarchies = FindAccountHierarchy(allAccount, account)
                });

            return hierarchy;
        }

        public async Task<List<Accounts>> GetAccountByColleagueAsync(StaffQueryModel model)
        {
            var items = await _context.Accounts
               .Include(t => t.AccountPhoneContacts)
               .Include(t => t.AccountProfile)
               .OrderBy(p => p.FullName)
               .Where(p =>
               p.FullName.Contains(model.Colleague ?? "") ||
               p.Email.Contains(model.Colleague ?? "") ||
               p.AccountProfile.JobTitle.Contains(model.Colleague ?? "")
               )
               .Take(model.MaxResult)
               .ToListAsync();

            return items;
        }

        public async Task<Accounts> GetAccountByEmailAsync(string email)
        {
            return await _context.Accounts
               .Where(c => c.Email == email)
               .FirstOrDefaultAsync();
        }

        public async Task<List<Accounts>> GetAccountByFilterAsync(StaffQueryModel model)
        {
            var items = await _context.Accounts
               .Include(t => t.AccountPhoneContacts)
               .Include(t => t.AccountProfile)
               .OrderBy(p => p.FullName)
               .Where(p =>
               p.AccountProfile.CompanyName.Equals(model.Company) ||
               p.AccountProfile.Division.Equals(model.Division) ||
               p.AccountProfile.Department.Contains(model.Department)
               )
               .Take(model.MaxResult)
               .ToListAsync();

            return items;
        }

        public async Task<Accounts> GetAccountByIdAsync(string id)
        {
            return await _context.Accounts
                .Where(c => c.Id == id)
                .Include(i => i.AccountPhoneContacts)
                .Include(i => i.AccountProfile)
                .FirstOrDefaultAsync();
        }

        public List<Accounts> GetAccountHierarchyById(string userId)
        {
            var parentAccount = _context.Accounts.Where(c => c.Id == userId).FirstOrDefault();
            var accountsHierarchy = FindAccountHierarchy(_context.Accounts.Include(p => p.AccountProfile).AsEnumerable(), parentAccount);

            List<Accounts> accountList = new List<Accounts>();

            // add parent account
            accountList.Add(parentAccount);

            // add structure

            foreach (var account in accountsHierarchy)
            {
                accountList.Add(account.accounts);
                if (account.accountHierarchies.Any())
                {
                    foreach (var hierarchy in account.accountHierarchies)
                    {
                        accountList.Add(hierarchy.accounts);
                    }
                }
            }

            return accountList;
        }

        public PagedList<Accounts> GetAllAccount(PagingParams pagingParams)
        {
            var query = _context.Accounts
                .Include(t => t.AccountPhoneContacts)
                .Include(t => t.AccountProfile)
                .OrderBy(p => p.FullName)
                .AsQueryable();
            return new PagedList<Accounts>(query, pagingParams.PageNumber, pagingParams.PageSize);
        }

        public async Task<List<Accounts>> GetAllAccountAsync(int row = 100)
        {
            var items = await _context.Accounts
                .Include(t => t.AccountPhoneContacts)
                .Include(t => t.AccountProfile)
                .OrderBy(p => p.FullName)
                .Take(row)
                .ToListAsync();

            return items;
        }

        public async Task<InitialFilterOutputModel> GetInitialFilterAsync()
        {
            return new InitialFilterOutputModel
            {
                Companies = await _context.AccountProfiles
               .Select(c => c.CompanyName).Distinct().ToListAsync(),
                Divisions = await _context.AccountProfiles
               .Select(c => c.Division).Distinct().ToListAsync(),
                Departments = await _context.AccountProfiles
               .Select(c => c.Department).Distinct().ToListAsync(),
            };
        }
    }
}
