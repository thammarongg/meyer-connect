﻿using Core.Backend.Models.Analytic;
using Core.Backend.Services.Google;
using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;
using Google.Apis.AnalyticsReporting.v4;
using Google.Apis.AnalyticsReporting.v4.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace Core.Backend.Services
{
    public interface IGoogleAnalyticService
    {
        List<TrackingAnalyticModel> GetAnalyticData(bool isFilterUserId = false);
        List<TrackingAnalyticModel> GetAnalyticDataByUser(int wikiUserId);
    }

    public class GoogleAnalyticService : IGoogleAnalyticService
    {
        private IHostingEnvironment _hostingEnvironment;

        public GoogleAnalyticService(IHostingEnvironment environment)
        {
            _hostingEnvironment = environment;
        }

        public List<TrackingAnalyticModel> GetAnalyticData(bool isFilterUserId = false)
        {
            try
            {
                // Get active credential
                string credPath = "MeyerConnect-46de1fe65687.json";

                var json = File.ReadAllText(credPath);
                var cr = JsonConvert.DeserializeObject<GooglePersonalServiceAccountModel>(json); // "personal" service account credential

                // Create an explicit ServiceAccountCredential credential
                var xCred = new ServiceAccountCredential(new ServiceAccountCredential.Initializer(cr.client_email)
                {
                    Scopes = new[] {
                        AnalyticsService.Scope.Analytics,
                        AnalyticsService.Scope.AnalyticsManageUsersReadonly,
                        AnalyticsService.Scope.AnalyticsReadonly
                    }
                }.FromPrivateKey(cr.private_key));

                // Create the service
                AnalyticsService service = new AnalyticsService(
                    new BaseClientService.Initializer()
                    {
                        HttpClientInitializer = xCred,
                    }
                );

                // Create the service
                AnalyticsReportingService reportingService = new AnalyticsReportingService(
                    new BaseClientService.Initializer()
                    {
                        HttpClientInitializer = xCred,
                    }
                );

                // GA Data Feed query uri.
                //string profileId = "ga:173986594";
                //string startDate = "2018-04-01";
                //string endDate = "2018-12-30";
                //string metrics = "ga:sessions,ga:users,ga:pageviews,ga:bounceRate,ga:visits";
                //DataResource.GaResource.GetRequest request = service.Data.Ga.Get(profileId, startDate, endDate, metrics);
                //GaData data = request.Execute();

                DateRange dateRange = new DateRange() { StartDate = "2018-01-01", EndDate = "2018-12-31" };
                Metric timeOnPage = new Metric { Expression = "ga:timeOnPage" };
                Dimension pagePath = new Dimension { Name = "ga:pagePath" };
                Dimension pageTitle = new Dimension { Name = "ga:pageTitle" };
                Dimension dimension8 = new Dimension { Name = "ga:dimension8" };
                Dimension date = new Dimension { Name = "ga:date" };
                Dimension hour = new Dimension { Name = "ga:hour" };
                Dimension minute = new Dimension { Name = "ga:minute" };

                // Create the ReportRequest object.
                ReportRequest reportRequest = new ReportRequest
                {
                    ViewId = "173986594",
                    DateRanges = new List<DateRange> { dateRange },
                    Dimensions = new List<Dimension> { pagePath, pageTitle, dimension8, date, hour, minute },
                    Metrics = new List<Metric> { timeOnPage },
                    //OrderBys = new List<OrderBy> { new OrderBy() { FieldName = "ga:date", SortOrder = "-" } }
                };

                var getReportsRequest = new GetReportsRequest
                {
                    ReportRequests = new List<ReportRequest> { reportRequest }
                };
                var batchRequest = reportingService.Reports.BatchGet(getReportsRequest);
                var response = batchRequest.Execute();
                var trackingAnalyticList = new List<TrackingAnalyticModel>();

                foreach (var x in response.Reports.First().Data.Rows)
                {
                    var textBuilder = new StringBuilder(x.Dimensions[3]);
                    textBuilder.Insert(4, "-");
                    textBuilder.Insert(7, "-");
                    var tempConvertDate = string.Format("{0} {1}:{2}", textBuilder.ToString(), x.Dimensions[4], x.Dimensions[5]);

                    var trackingAnalytic = new TrackingAnalyticModel()
                    {
                        pagePath = x.Dimensions[0],
                        pageTitle = x.Dimensions[1],
                        userId = int.Parse(x.Dimensions[2]),
                        activeDate = DateTime.Parse(tempConvertDate),
                        hour = int.Parse(x.Dimensions[4]),
                        minute = int.Parse(x.Dimensions[5]),
                        timeOnPage = double.Parse(x.Metrics.First().Values[0])
                    };

                    trackingAnalyticList.Add(trackingAnalytic);
                    Console.WriteLine(string.Join(", ", x.Dimensions) + "   " + string.Join(", ", x.Metrics.First().Values));
                }

                //List<ReportRequest> requests = new List<ReportRequest>();
                //requests.Add(reportRequest);

                //// Create the GetReportsRequest object.
                //GetReportsRequest getReport = new GetReportsRequest() { ReportRequests = requests };

                //// Call the batchGet method.
                //GetReportsResponse response = reportingService.Reports.BatchGet(getReport).Execute();


                // some calls to Google API
                //var act1 = service.Management.Accounts.List().Execute();

                //var actSum = service.Management.AccountSummaries.List().Execute();

                //var resp1 = service.Management.Profiles.List(actSum.Items[0].Id, actSum.Items[0].WebProperties[0].Id).Execute();

                return trackingAnalyticList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<TrackingAnalyticModel> GetAnalyticDataByUser(int wikiUserId)
        {
            try
            {
                // Get active credential
                string credPath = "MeyerConnect-46de1fe65687.json";

                var json = File.ReadAllText(credPath);
                var cr = JsonConvert.DeserializeObject<GooglePersonalServiceAccountModel>(json); // "personal" service account credential

                // Create an explicit ServiceAccountCredential credential
                var xCred = new ServiceAccountCredential(new ServiceAccountCredential.Initializer(cr.client_email)
                {
                    Scopes = new[] {
                        AnalyticsService.Scope.Analytics,
                        AnalyticsService.Scope.AnalyticsManageUsersReadonly,
                        AnalyticsService.Scope.AnalyticsReadonly
                    }
                }.FromPrivateKey(cr.private_key));

                // Create the service
                AnalyticsService service = new AnalyticsService(
                    new BaseClientService.Initializer()
                    {
                        HttpClientInitializer = xCred,
                    }
                );

                // Create the service
                AnalyticsReportingService reportingService = new AnalyticsReportingService(
                    new BaseClientService.Initializer()
                    {
                        HttpClientInitializer = xCred,
                    }
                );

                // GA Data Feed query uri.
                //string profileId = "ga:173986594";
                //string startDate = "2018-04-01";
                //string endDate = "2018-12-30";
                //string metrics = "ga:sessions,ga:users,ga:pageviews,ga:bounceRate,ga:visits";
                //DataResource.GaResource.GetRequest request = service.Data.Ga.Get(profileId, startDate, endDate, metrics);
                //GaData data = request.Execute();

                DateRange dateRange = new DateRange() { StartDate = "2018-01-01", EndDate = "2018-12-31" };
                Metric timeOnPage = new Metric { Expression = "ga:timeOnPage" };
                Dimension pagePath = new Dimension { Name = "ga:pagePath" };
                Dimension pageTitle = new Dimension { Name = "ga:pageTitle" };
                Dimension dimension8 = new Dimension { Name = "ga:dimension8" };
                Dimension date = new Dimension { Name = "ga:date" };
                Dimension hour = new Dimension { Name = "ga:hour" };
                Dimension minute = new Dimension { Name = "ga:minute" };

                // Create the ReportRequest object.
                ReportRequest reportRequest = new ReportRequest
                {
                    ViewId = "173986594",
                    DateRanges = new List<DateRange> { dateRange },
                    Dimensions = new List<Dimension> { pagePath, pageTitle, dimension8, date, hour, minute },
                    Metrics = new List<Metric> { timeOnPage },
                    FiltersExpression = string.Format("ga:dimension8=={0}", wikiUserId)
                    //OrderBys = new List<OrderBy> { new OrderBy() { FieldName = "ga:date", SortOrder = "-" } }
                };

                var getReportsRequest = new GetReportsRequest
                {
                    ReportRequests = new List<ReportRequest> { reportRequest }
                };
                var batchRequest = reportingService.Reports.BatchGet(getReportsRequest);
                var response = batchRequest.Execute();
                var trackingAnalyticList = new List<TrackingAnalyticModel>();

                foreach (var x in response.Reports.First().Data.Rows)
                {
                    var textBuilder = new StringBuilder(x.Dimensions[3]);
                    textBuilder.Insert(4, "-");
                    textBuilder.Insert(7, "-");
                    var tempConvertDate = string.Format("{0} {1}:{2}", textBuilder.ToString(), x.Dimensions[4], x.Dimensions[5]);

                    var trackingAnalytic = new TrackingAnalyticModel()
                    {
                        pagePath = x.Dimensions[0],
                        pageTitle = x.Dimensions[1],
                        userId = int.Parse(x.Dimensions[2]),
                        activeDate = DateTime.Parse(tempConvertDate),
                        hour = int.Parse(x.Dimensions[4]),
                        minute = int.Parse(x.Dimensions[5]),
                        timeOnPage = double.Parse(x.Metrics.First().Values[0])
                    };

                    trackingAnalyticList.Add(trackingAnalytic);
                    Console.WriteLine(string.Join(", ", x.Dimensions) + "   " + string.Join(", ", x.Metrics.First().Values));
                }

                //List<ReportRequest> requests = new List<ReportRequest>();
                //requests.Add(reportRequest);

                //// Create the GetReportsRequest object.
                //GetReportsRequest getReport = new GetReportsRequest() { ReportRequests = requests };

                //// Call the batchGet method.
                //GetReportsResponse response = reportingService.Reports.BatchGet(getReport).Execute();


                // some calls to Google API
                //var act1 = service.Management.Accounts.List().Execute();

                //var actSum = service.Management.AccountSummaries.List().Execute();

                //var resp1 = service.Management.Profiles.List(actSum.Items[0].Id, actSum.Items[0].WebProperties[0].Id).Execute();

                return trackingAnalyticList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}