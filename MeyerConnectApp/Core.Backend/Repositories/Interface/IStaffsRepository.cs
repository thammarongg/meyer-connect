﻿using Core.Backend.Models.Ltes;
using Core.Backend.Models.Staff;
using Core.Backend.Models.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Repositories.Interface
{
    public interface IStaffsRepository
    {
        Task<List<Accounts>> GetAll(int rows = int.MaxValue);
        PagedList<Accounts> GetAll(PagingParams pagingParams);
        Task<List<Accounts>> GetByColleague(StaffQueryModel model);
        Task<List<Accounts>> GetByFilter(StaffQueryModel model);
        Task<Dictionary<string, bool>> UpdateStaffTweetStatus(StaffEventsQueryModel model);
        Task<Accounts> GetById(string id);
        Task<InitialFilterOutputModel> GetInitialFilter();
        Task<Accounts> GetByEmail(string email);

        IEnumerable<AccountHierarchyModel> FindAccountHierarchy(IEnumerable<Accounts> allAccount, Accounts parentAccount);
        List<Accounts> GetAccountHierarchyById(string userId);
    }
}
