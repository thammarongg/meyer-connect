﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Backend.Models.Staff
{
    public class AccountProfile
    {
        public string EmployeeId { get; set; }

        [Required]
        public string JobTitle { get; set; }

        [Required]
        public string CompanyName { get; set; }

        [Required]
        public string Division { get; set; }

        [Required]
        public string Department { get; set; }

        [Required]
        public string City { get; set; }

        public string SkypeId { get; set; }
        public string ManagerEmail { get; set; }

        public string IntroduceMessage { get; set; }
        public int ProfileStatus { get; set; }
        public DateTime VacationDateUtc { get; set; }
        public string BusinessTripLocation { get; set; }
        public string BusinessTripEmailContact { get; set; }
        public string BusinessTripPhoneContact { get; set; }
        public string BusinessTripNote { get; set; }

        [Key]
        public string UserId { get; set; }

        [NotMapped]
        public Accounts Accounts { get; set; }
    }
}