﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Models.Wiki
{
    public class WikiContentViewStat
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty("id")]
        public int Id { get; set; }
        [Required]
        [JsonProperty("postId")]
        public string PostId { get; set; }
        [Required]
        [JsonProperty("userId")]
        public string UserId { get; set; }
        [Required]
        [JsonProperty("transactionDateUtc")]
        public DateTime TransactionDateUtc { get; set; } = DateTime.UtcNow;



    }
}
