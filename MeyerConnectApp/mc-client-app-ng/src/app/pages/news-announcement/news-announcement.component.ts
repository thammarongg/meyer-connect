import { environment } from './../../../environments/environment';
import { TruncateModule } from 'ng2-truncate';
import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { NewsAnnouncementService } from '../../services/news-announcement.service';
import { HubConnection } from '@aspnet/signalr';

import * as moment from 'moment';
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-news-announcement',
  templateUrl: './news-announcement.component.html',
  styleUrls: ['./news-announcement.component.css']
})
export class NewsAnnouncementComponent implements OnInit, OnChanges {
  announcements: any[];
  limit = 10;
  truncating = true;
  private _hubConnection: HubConnection;

  constructor(private newsAnnoucementService: NewsAnnouncementService) {}

  ngOnInit() {
    $(document).ready(function() {
      $('span.date').popup();
    });
    this._hubConnection = new HubConnection(environment.hubUrl);
    this._hubConnection
      .start()
      .then(() =>
        this._hubConnection.invoke('GetAllPost').catch(err => console.log(err))
      )
      .catch(err => console.log('Error while establishing connection :('));

    this._hubConnection.on('GetAllPost', (data: any) => {
      this.getAnnouncement();
    });

    // this.getAnnouncement();
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('call');
  }

  getAnnouncement(): void {
    this.newsAnnoucementService.getAnnouncements().subscribe((data: any) => {
      if (data) {
        this.announcements = data.items;
      }
    });
  }

  private setTruncating(isTruncating: boolean): void {
    this.truncating = isTruncating;
  }

  filterAnnouncementItems(type: number) {
    if (!this.announcements) {
      return [];
    }
    return this.announcements.filter(x => x.PostType === type);
  }
}
