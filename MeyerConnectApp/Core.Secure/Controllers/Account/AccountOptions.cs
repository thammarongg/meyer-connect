﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Secure.Controllers.Account
{
    public class AccountOptions
    {
        public static bool ShowLogoutPrompt = false;
        public static bool AutomaticRedirectAfterSignOut = true;
    }
}
