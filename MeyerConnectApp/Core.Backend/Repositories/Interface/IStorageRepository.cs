﻿using Core.Backend.Models.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Repositories.Interface
{
    public interface IStorageRepository
    {
        List<Storages> GetAllFile();
        Storages GetFileById(string fileId);

        Task<bool> CreateFileDataAsync(Storages storages);
        Task<bool> UpdateFileDataAsync(Storages storages);
        Task<bool> DeleteFileDataAsync(Storages storages);
    }
}
