﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Secure.Models.Account
{
    public class LogoutInputModel
    {
        public string LogoutId { get; set; }
        public string RedirectUrl { get; set; }
    }
}
