﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Core.Backend.Data.Migrations.MeyerConnectAppDb
{
    public partial class DeletePostTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PostAudiences");

            migrationBuilder.DropTable(
                name: "Posts");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Contents = table.Column<string>(nullable: false),
                    CreatedPostBy = table.Column<string>(nullable: false),
                    CreatedPostDateUtc = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    FromSubject = table.Column<string>(nullable: false),
                    IsApproved = table.Column<bool>(nullable: false),
                    IsPublish = table.Column<bool>(nullable: false),
                    IsSchedulePublish = table.Column<bool>(nullable: false),
                    LastModifiedBy = table.Column<string>(nullable: false),
                    LastModifiedDateUtc = table.Column<DateTime>(nullable: false),
                    PostType = table.Column<int>(nullable: false),
                    PublishDate = table.Column<DateTime>(nullable: false),
                    Revise = table.Column<int>(nullable: false),
                    TitleName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PostAudiences",
                columns: table => new
                {
                    Seq = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Group = table.Column<string>(nullable: true),
                    IsAllGroup = table.Column<bool>(nullable: false),
                    Level = table.Column<string>(nullable: true),
                    PostId = table.Column<int>(nullable: false),
                    Team = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostAudiences", x => x.Seq);
                    table.ForeignKey(
                        name: "FK_PostAudiences_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PostAudiences_PostId",
                table: "PostAudiences",
                column: "PostId");
        }
    }
}
