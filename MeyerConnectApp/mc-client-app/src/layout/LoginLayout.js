import React from "react"

const LoginLayout = ({ children, ...rest }) => (
    <div className="layout">
        <div className="layout__login">
            {children}
        </div>
    </div>
)

export default LoginLayout