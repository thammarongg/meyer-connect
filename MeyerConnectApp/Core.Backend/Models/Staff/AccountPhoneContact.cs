﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Backend.Models.Staff
{
    public class AccountPhoneContact
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Seq { get; set; }

        [Required]
        public int PhoneType { get; set; }

        [Required]
        public string CountryCode { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        public string Extension { get; set; }

        public string UserId { get; set; }
        [NotMapped]
        public Accounts Accounts { get; set; }
    }
}