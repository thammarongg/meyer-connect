﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Models.Staff
{
    public class InitialFilterOutputModel
    {
        public List<string> Companies { get; set; }
        public List<string> Divisions { get; set; }
        public List<string> Departments { get; set; }
    }
}
