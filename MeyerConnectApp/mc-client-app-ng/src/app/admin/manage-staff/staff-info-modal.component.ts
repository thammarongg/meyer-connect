import {
  Component,
  AfterViewInit,
  EventEmitter,
  Output,
  Input,
  OnInit
} from '@angular/core';
import { StaffService } from '../../services/staff.service';

declare var $: any;

@Component({
  selector: 'staff-info-modal',
  template: `
        <div class="ui tiny modal" id="staff_info_modal_{{id}}">
            <i class="close icon"></i>
            <h2 class="ui header">
                <img src="https://semantic-ui.com/images/avatar2/large/patrick.png" class="ui circular image">
                <div class="content">
                    {{staff.FullName}}
                    <div class="sub header">{{staff.AccountProfile.JobTitle}}</div>
                </div>
            </h2>
            <div class="content">
                <div class="description">
                  <p></p>
                </div>
            </div>
        </div>
    `
})
export class StaffInfoModalComponent implements OnInit {
  @Output()
  public onDeActivateClicked: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output()
  public onEditClicked: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() staff: any;
  @Input() id;

  constructor(private staffService: StaffService) {}

  ngOnInit(): void {
    console.log(this.id);
  }

  //   ngAfterViewInit(): void {
  //     $('#staff_info_modal_' + this.id).modal('show');
  //   }
}
