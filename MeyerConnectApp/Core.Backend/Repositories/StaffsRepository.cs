﻿using Core.Backend.Models;
using Core.Backend.Models.Ltes;
using Core.Backend.Models.Staff;
using Core.Backend.Models.Utils;
using Core.Backend.Repositories.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions.Internal;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Repositories
{
    public class StaffsRepository : IStaffsRepository
    {
        private readonly AppDbContext _context;

        public StaffsRepository(
            AppDbContext context
            )
        {
            _context = context;
        }

        public async Task<List<Accounts>> GetAll(int rows = 100)
        {
            #region "highlight"

            //var items = await _context.Accounts
            //.Select(c => new
            //{
            //    c.Id,
            //    c.Email,
            //    c.UserName,
            //    c.LastLoginUtc,
            //    c.AccountProfileImageUrl,
            //    c.AccountStatus,
            //    c.PermissionRole,
            //    AccountPhoneContacts = c.AccountPhoneContacts.Select(a => new
            //    {
            //        a.Seq,
            //        a.CountryCode,
            //        a.Extension,
            //        a.PhoneNumber,
            //        a.PhoneType
            //    }).ToList(),
            //    AccountProfile = new
            //    {
            //        c.AccountProfile.EmployeeId,
            //        c.AccountProfile.JobTitle,
            //        c.AccountProfile.CompanyName,
            //        c.AccountProfile.Division,
            //        c.AccountProfile.Department,
            //        c.AccountProfile.City,
            //        c.AccountProfile.SkypeId,
            //        c.AccountProfile.ManagerEmail,
            //        c.AccountProfile.IntroduceMessage,
            //        c.AccountProfile.ProfileStatus,
            //        c.AccountProfile.VacationDateUtc,
            //        c.AccountProfile.BusinessTripLocation,
            //        c.AccountProfile.BusinessTripEmailContact,
            //        c.AccountProfile.BusinessTripPhoneContact,
            //        c.AccountProfile.BusinessTripNote
            //    }
            //}).ToListAsync();
            //var items = new List<Accounts>();

            //var result = from m in _context.Accounts
            //             select new
            //             {
            //                 Id = m.Id,
            //                 Email = m.Email,
            //                 UserName = m.UserName,
            //                 LastLoginUtc = m.LastLoginUtc,
            //                 AccountProfileImageUrl = m.AccountProfileImageUrl,
            //                 AccountStatus = m.AccountStatus,
            //                 PermissionRole = m.PermissionRole,
            //                 AccountPhoneContacts = from r in _context.AccountPhoneContacts
            //                                        where m.Id == r.UserId
            //                                        select new
            //                                        {
            //                                            CountryCode = r.CountryCode,
            //                                            Extension = r.Extension,
            //                                            PhoneNumber = r.PhoneNumber,
            //                                            PhoneType = r.PhoneType
            //                                        },
            //                 AccountProfile = from p in _context.AccountProfiles
            //                                  where m.Id == p.UserId
            //                                  select new
            //                                  {
            //                                      p.EmployeeId,
            //                                      p.JobTitle,
            //                                      p.CompanyName,
            //                                      p.Division,
            //                                      p.Department,
            //                                      p.City,
            //                                      p.SkypeId,
            //                                      p.ManagerEmail,
            //                                      p.IntroduceMessage,
            //                                      p.ProfileStatus,
            //                                      p.VacationDateUtc,
            //                                      p.BusinessTripLocation,
            //                                      p.BusinessTripEmailContact,
            //                                      p.BusinessTripPhoneContact,
            //                                      p.BusinessTripNote
            //                                  }

            //             };

            #endregion "highlight"

            var items = new List<Accounts>();
            items = await _context.Accounts
                .Include(t => t.AccountPhoneContacts)
                .Include(t => t.AccountProfile)
                .OrderBy(p => p.FullName)
                .Take(rows)
                .ToListAsync();

            //var items = new List<Accounts>();
            //items = await _context.Accounts
            //    .OrderBy(p => p.FullName)
            //    .Select(c => new Accounts
            //    {
            //        Id = c.Id,
            //        Email = c.Email,
            //        UserName = c.UserName,
            //        LastLoginUtc = c.LastLoginUtc,
            //        AccountProfileImageUrl = c.AccountProfileImageUrl,
            //        AccountStatus = c.AccountStatus,
            //        PermissionRole = c.PermissionRole,
            //        FullName = c.FullName,
            //        AccountPhoneContacts = c.AccountPhoneContacts.Select(a => new AccountPhoneContact
            //        {
            //            CountryCode = a.CountryCode,
            //            Extension = a.Extension,
            //            PhoneNumber = a.PhoneNumber,
            //            PhoneType = a.PhoneType
            //        }).ToList(),
            //        AccountProfile = new AccountProfile
            //        {
            //            EmployeeId = c.AccountProfile.EmployeeId,
            //            JobTitle = c.AccountProfile.JobTitle,
            //            CompanyName = c.AccountProfile.CompanyName,
            //            Division = c.AccountProfile.Division,
            //            Department = c.AccountProfile.Department,
            //            City = c.AccountProfile.City,
            //            SkypeId = c.AccountProfile.SkypeId,
            //            ManagerEmail = c.AccountProfile.ManagerEmail,
            //            IntroduceMessage = c.AccountProfile.IntroduceMessage,
            //            ProfileStatus = c.AccountProfile.ProfileStatus,
            //            VacationDateUtc = c.AccountProfile.VacationDateUtc,
            //            BusinessTripLocation = c.AccountProfile.BusinessTripLocation,
            //            BusinessTripEmailContact = c.AccountProfile.BusinessTripEmailContact,
            //            BusinessTripPhoneContact = c.AccountProfile.BusinessTripPhoneContact,
            //            BusinessTripNote = c.AccountProfile.BusinessTripNote
            //        }
            //    })
            //    .Take(rows)
            //    .ToListAsync();
            return items;
        }

        public PagedList<Accounts> GetAll(PagingParams pagingParams)
        {
            var query = _context.Accounts
                .Include(t => t.AccountPhoneContacts)
                .Include(t => t.AccountProfile)
                .OrderBy(p => p.FullName)
                .AsQueryable();
            return new PagedList<Accounts>(query, pagingParams.PageNumber, pagingParams.PageSize);
        }

        public async Task<List<Accounts>> GetByColleague(StaffQueryModel model)
        {
            var items = await _context.Accounts
                .Include(t => t.AccountPhoneContacts)
                .Include(t => t.AccountProfile)
                .OrderBy(p => p.FullName)
                .Where(p =>
                p.FullName.Contains(model.Colleague ?? "") ||
                p.Email.Contains(model.Colleague ?? "") ||
                p.AccountProfile.JobTitle.Contains(model.Colleague ?? "")
                )
                .Take(model.MaxResult)
                .ToListAsync();

            return items;
        }

        public async Task<List<Accounts>> GetByFilter(StaffQueryModel model)
        {
            var items = await _context.Accounts
                .Include(t => t.AccountPhoneContacts)
                .Include(t => t.AccountProfile)
                .OrderBy(p => p.FullName)
                .Where(p =>
                p.AccountProfile.CompanyName.Equals(model.Company) ||
                p.AccountProfile.Division.Equals(model.Division) ||
                p.AccountProfile.Department.Contains(model.Department)
                )
                .Take(model.MaxResult)
                .ToListAsync();

            return items;            
        }

        public async Task<Accounts> GetById(string id)
        {
            return await _context.Accounts
                .Where(c => c.Id == id)
                .Include(i => i.AccountPhoneContacts)
                .Include(i => i.AccountProfile)
                .FirstOrDefaultAsync();
        }

        public async Task<Accounts> GetByEmail(string email)
        {
            return await _context.Accounts
                .Where(c => c.Email == email)
                .FirstOrDefaultAsync();
        }

        public async Task<InitialFilterOutputModel> GetInitialFilter()
        {
            return new InitialFilterOutputModel
            {
                Companies = await _context.AccountProfiles
                .Select(c => c.CompanyName).Distinct().ToListAsync(),
                Divisions = await _context.AccountProfiles
                .Select(c => c.Division).Distinct().ToListAsync(),
                Departments = await _context.AccountProfiles
                .Select(c => c.Department).Distinct().ToListAsync(),
            };
        }

        public async Task<Dictionary<string, bool>> UpdateStaffTweetStatus(StaffEventsQueryModel model)
        {
            // defind
            var dict = new Dictionary<string, bool>() {
                { "HasUserProfile", false },
                { "HasUpdated", false }
            };

            // check user id
            var user = await _context.Accounts.FindAsync(model.UserId);
            if (user == null)
            {
                dict["HasUserProfile"] = false;
                return dict;
            }
            else
            {
                dict["HasUserProfile"] = true;
            }

            // update status
            user.AccountProfile = _context.AccountProfiles.Where(c => c.UserId == model.UserId).FirstOrDefault();
            user.AccountProfile.IntroduceMessage = model.TweetStatus;
            await _context.SaveChangesAsync();

            dict["HasUpdated"] = true;
            return dict;
        }

        public IEnumerable<AccountHierarchyModel> FindAccountHierarchy(IEnumerable<Accounts> allAccount, Accounts parentAccount)
        {
            string parentUserId = null;
            string parentEmail = null;

            if (parentAccount != null)
            {
                parentUserId = parentAccount.Id;
                parentEmail = parentAccount.Email;
            }

            var childAccount = allAccount.Where(e => e.AccountProfile.ManagerEmail == parentEmail);

            Collection<AccountHierarchyModel> hierarchy = new Collection<AccountHierarchyModel>();

            foreach (var account in childAccount)

                hierarchy.Add(new AccountHierarchyModel()
                {
                    accounts = account,
                    accountHierarchies = FindAccountHierarchy(allAccount, account)
                });

            return hierarchy;
        }

        public List<Accounts> GetAccountHierarchyById(string userId)
        {
            var parentAccount = _context.Accounts.Where(c => c.Id == userId).FirstOrDefault();
            var accountsHierarchy = FindAccountHierarchy(_context.Accounts.Include(p => p.AccountProfile).AsEnumerable(), parentAccount);

            List<Accounts> accountList = new List<Accounts>();

            // add parent account
            accountList.Add(parentAccount);

            // add structure

            foreach (var account in accountsHierarchy)
            {
                accountList.Add(account.accounts);
                if (account.accountHierarchies.Any())
                {
                    foreach (var hierarchy in account.accountHierarchies)
                    {
                        accountList.Add(hierarchy.accounts);
                    }
                }
            }

            return accountList;
        }
    }
}