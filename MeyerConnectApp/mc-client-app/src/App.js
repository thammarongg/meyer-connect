import React from "react"
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch
} from "react-router-dom"
import { HomeBaseRoute } from "./routes"
import { HomePage, CallbackPage } from "./views"

const App = () => (
  <div className="App">
    <Router>
      <Switch>
        <Route exact path="/">
          <Redirect to="/home" />
        </Route>
        <HomeBaseRoute path="/home" component={HomePage} />
        <Route path="/callback" component={CallbackPage} />
      </Switch>
    </Router>
  </div>
)

export default App
