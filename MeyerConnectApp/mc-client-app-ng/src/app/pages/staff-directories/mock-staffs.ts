import { StaffPhone } from './staff-phone';
import { Staff } from './staff';

export const StaffList: Staff[] = [
    { EmployeeId: '011557',
    Email: 'thammarongg@meyer-mil.com',
    JobTitle: 'Sr.Programmer',
    FullName: 'Thammarong Glomjai',
    Division: 'MIL-Office',
    CompanyName: 'MIL',
    Department: 'Management Information System',
    City: 'Thailand',
    Phones: [
        {PhoneType: 1, PhoneNumber: '038404200', CountryCode: '+66'},
        {PhoneType: 2, PhoneNumber: '0870140412', CountryCode: '+66'}
    ],
    AccountProfileImageUrl: 'https://semantic-ui.com/images/avatar/large/elliot.jpg' },
    { EmployeeId: 'T02244',
    Email: 'sarah@meyer-mil.com',
    JobTitle: 'QA Assistant',
    FullName: 'Sara Hit',
    Division: 'MTL',
    CompanyName: 'MTL',
    Department: 'QA',
    City: 'Thailand',
    Phones: [
        {PhoneType: 1, PhoneNumber: '038404200', CountryCode: '+66'},
        {PhoneType: 2, PhoneNumber: '0870140412', CountryCode: '+66'}
    ],
    AccountProfileImageUrl: 'https://semantic-ui.com/images/avatar/large/elliot.jpg' },
];
