﻿using Core.Backend.Models.Post;
using Core.Backend.Models.Staff;
using Core.Backend.Models.Storage;
using Microsoft.EntityFrameworkCore;

namespace Core.Backend.Models
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        public DbSet<Accounts> Accounts { get; set; }
        public DbSet<AccountProfile> AccountProfiles { get; set; }
        public DbSet<AccountPhoneContact> AccountPhoneContacts { get; set; }

        public DbSet<Posts> Posts { get; set; }
        public DbSet<PostAudiences> PostAudiences { get; set; }
        public DbSet<PostAttachments> PostAttachments { get; set; }

        public DbSet<Storages> Storages { get; set; }


    protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Accounts>()
               .HasMany(p => p.AccountPhoneContacts)
               .WithOne(p => p.Accounts)
               .HasForeignKey(b => b.UserId)
               .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Accounts>()
                .HasOne(p => p.AccountProfile)
                .WithOne(p => p.Accounts)
                .HasForeignKey<AccountProfile>(b => b.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Posts>()
                .HasMany(p => p.PostAudiences)
                .WithOne(p => p.Posts)
                .HasForeignKey(b => b.PostId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Posts>()
                .HasMany(p => p.PostAttachments)
                .WithOne(p => p.Posts)
                .HasForeignKey(b => b.PostId)
                .OnDelete(DeleteBehavior.Cascade);


            builder.Entity<AccountProfile>().ToTable("AccountProfile");
            builder.Entity<AccountPhoneContact>().ToTable("AccountPhoneContact");
        }
    }
}