﻿using MailKit.Net.Smtp;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using MimeKit;
using System.Threading.Tasks;

namespace Core.Secure.Services
{
    public class EmailSender : IEmailSender
    {
        public EmailSender(IOptions<EmailSenderOptions> optionsAccessor)
        {
            options = optionsAccessor.Value;
        }

        private EmailSenderOptions options { get; }

        public async Task SendEmailAsync(string email, string subject, string message, bool isHtml = true)
        {
            var mime = new MimeMessage();

            mime.From.Add(new MailboxAddress("MeyerConnect", "support@meyer-connect.com"));
            mime.To.Add(new MailboxAddress(email));
            mime.Subject = subject;
            mime.Body = new TextPart("html")
            {
                Text = message
            };

            await Task.Run(() =>
            {
                using (var client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect(options.SmtpHost, options.Port, options.EnableSSL);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(options.SmtpUserName, options.SmtpPassword);

                    client.Send(mime);
                    client.Disconnect(true);
                }
            });
        }
    }
}