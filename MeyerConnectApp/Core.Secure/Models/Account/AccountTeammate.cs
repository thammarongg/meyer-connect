﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Secure.Models.Account
{
    public class AccountTeammate
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Seq { get; set; }

        public string TeammateEmail { get; set; }

        public string UserId { get; set; }
        public Accounts Accounts { get; set; }
    }
}
