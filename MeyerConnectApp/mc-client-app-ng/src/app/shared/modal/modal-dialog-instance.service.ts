import { ComponentRef } from '@angular/core';
import { ModalStaffInfoComponent } from './modal-staff-info.component';

export class ModalDialogInstanceService {
  /**
   * Used to make sure there is exactly one instance of Modal Dialog
   */
  private componentRef: ComponentRef<ModalStaffInfoComponent>;

  /**
   * Closes existing modal dialog
   */
  closeAnyExistingModalDialog() {
    if (this.componentRef) {
      this.componentRef.destroy();
    }
  }

  /**
   * Save component ref for future comparison
   * @param componentRef
   */
  saveExistingModalDialog(componentRef: ComponentRef<ModalStaffInfoComponent>) {
    this.componentRef = componentRef;
  }
}
