"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _semanticUiReact = require("semantic-ui-react");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = "C:\\Projects\\BitBucket\\meyer-connect\\MeyerConnectApp\\client-app-nextjs\\components\\profile\\profileMenu.js";


var anonymousProfileImage = "//semantic-ui.com/images/wireframe/square-image.png";

exports.default = function () {
  return _react2.default.createElement(_semanticUiReact.Image, { src: anonymousProfileImage, size: "mini", circular: true, __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    }
  });
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHNcXHByb2ZpbGVcXHByb2ZpbGVNZW51LmpzIl0sIm5hbWVzIjpbIkltYWdlIiwiYW5vbnltb3VzUHJvZmlsZUltYWdlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEsQUFBUzs7Ozs7OztBQUVULElBQU0sd0JBQU4sQUFDRSxBQUVGOztrQkFBZSxZQUFBO3lCQUFRLEFBQUMsd0NBQU0sS0FBUCxBQUFZLHVCQUF1QixNQUFuQyxBQUF3QyxRQUFPLFVBQS9DO2dCQUFBO2tCQUFSLEFBQVE7QUFBQTtHQUFBO0FBQXZCIiwiZmlsZSI6InByb2ZpbGVNZW51LmpzIiwic291cmNlUm9vdCI6IkM6L1Byb2plY3RzL0JpdEJ1Y2tldC9tZXllci1jb25uZWN0L01leWVyQ29ubmVjdEFwcC9jbGllbnQtYXBwLW5leHRqcyJ9