export const environment = {
  production: true,
  secureUrl: 'https://secure.meyer-connect.com',
  apiUrl: 'https://api.meyer-connect.com',
  enableTracing: false,
  viewAccessToken: false,
  hubUrl: 'https://api.meyer-connect.com/PostHub'
};
