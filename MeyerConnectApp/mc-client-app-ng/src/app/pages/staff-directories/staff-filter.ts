export class StaffFilter {
  company: string;
  division: string;
  department: string;
}
