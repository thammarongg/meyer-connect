﻿using Core.Backend.Hubs;
using Core.Backend.Models.Post;
using Core.Backend.Repositories.Interface;
using Microsoft.AspNetCore.SignalR;
using System;
using System.IO;
using TableDependency.Enums;
using TableDependency.EventArgs;
using TableDependency.SqlClient;
using TableDependency;

namespace Core.Backend.Services.Subscription
{
    public interface IDatabaseSubscription
    {
        void Configure(string connectionString = @"data source=MILDB;initial catalog=MeyerConnectApp;user id=sa;password=sql@Meyer");
    }

    public class PostDatabaseSubscription : IDatabaseSubscription
    {
        private bool disposedValue = false;
        private readonly IPostRepository _repository;
        private readonly IHubContext<PostHub> _hubContext;
        private SqlTableDependency<Posts> _tableDependency;

        public PostDatabaseSubscription(IPostRepository repository, IHubContext<PostHub> hubContext)
        {
            _repository = repository;
            _hubContext = hubContext;
        }

        public void Configure(string connectionString)
        {
            _tableDependency = new SqlTableDependency<Posts>(connectionString, null, null, null, null, null, DmlTriggerType.All);
            _tableDependency.OnChanged += Changed;
            _tableDependency.OnError += TableDependency_OnError;
            _tableDependency.Start();

            Console.WriteLine("Waiting for receiving notifications...");
        }

        private void TableDependency_OnError(object sender, TableDependency.EventArgs.ErrorEventArgs e)
        {
            Console.WriteLine($"SqlTableDependency error: {e.Error.Message}");
        }

        private void Changed(object sender, RecordChangedEventArgs<Posts> e)
        {
            if (e.ChangeType != ChangeType.None)
            {
                // TODO: manage the changed entity
                var changedEntity = e.Entity;
                _hubContext.Clients.All.SendAsync("GetAllPost", _repository.GetAllPostAsync());
            }
        }

        #region IDisposable

        ~PostDatabaseSubscription()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _tableDependency.Stop();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion IDisposable
    }
}