﻿using Core.Secure.Models;
using IdentityModel;
using IdentityServer4;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Core.Secure.Services
{
    public class CustomUserClaimsPrincipalFactory : UserClaimsPrincipalFactory<Accounts>
    {
        private readonly AuthDbContext _authDbContext;

        public CustomUserClaimsPrincipalFactory(
        UserManager<Accounts> userManager,
        AuthDbContext authDbContext,
        IOptions<IdentityOptions> optionsAccessor)
            : base(userManager, optionsAccessor)
        {
            _authDbContext = authDbContext;
        }

        public async override Task<ClaimsPrincipal> CreateAsync(Accounts user)
        {

            var principal = await base.CreateAsync(user);
            ((ClaimsIdentity)principal.Identity).AddClaims(new[] {
                //new Claim(JwtClaimTypes.Subject, user.Id),
                //new Claim(JwtClaimTypes.PreferredUserName, user.UserName),
                new Claim(JwtClaimTypes.Picture, user.AccountProfileImageUrl),
                new Claim(JwtClaimTypes.Name, user.FullName),
                new Claim(JwtClaimTypes.Role,  user.DefaultRole),
                new Claim(JwtClaimTypes.Scope, "mcPermissionRoles")
                //new Claim(IdentityServerConstants.StandardScopes.Email, user.Email)
            });
            return principal;
        }
    }
}