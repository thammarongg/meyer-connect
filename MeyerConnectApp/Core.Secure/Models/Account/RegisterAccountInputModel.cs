﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Core.Secure.Models.Account
{
    public class RegisterAccountInputModel
    {
        [Required]
        public string EmployeeId { get; set; }

        public string JobTitle { get; set; }

        [Required]
        public string CompanyName { get; set; }

        [Required]
        public string Division { get; set; }

        [Required]
        public string Department { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string FullName { get; set; }

        public string SkypeId { get; set; }
        public string ManagerEmail { get; set; }

        [Required]
        public string PermissionRole { get; set; }

        public string AccountProfileImageUrl { get; set; }

        public virtual ICollection<AccountPhoneContact> AccountPhoneContacts { get; set; }
    }
}