﻿using Core.Secure.Controllers.Home;
using Core.Secure.Models;
using Core.Secure.Models.Account;
using Core.Secure.Services;
using CsvHelper;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Core.Secure.Controllers.Account
{
    //[Authorize]
    [SecurityHeader]
    public class AccountController : Controller
    {
        private readonly UserManager<Accounts> _userManager;
        private readonly SignInManager<Accounts> _signInManager;

        //private readonly RoleManager<IdentityRole> _roleManager;
        private readonly AuthDbContext _authDbContext;

        private readonly ILogger _logger;

        private readonly IIdentityServerInteractionService _interaction;
        private readonly AccountService _account;

        private readonly IEmailSender _emailSender;

        public AccountController(
            ILogger<AccountController> logger,
            IIdentityServerInteractionService interaction,
            IHttpContextAccessor httpContextAccessor,
            IAuthenticationSchemeProvider schemeProvider,
            UserManager<Accounts> userManager,
            SignInManager<Accounts> signInManager,
            //RoleManager<IdentityRole> roleManager,
            AuthDbContext authDbContext,
            IEmailSender emailSender
            )
        {
            _interaction = interaction;
            _logger = logger;
            _account = new AccountService(interaction, httpContextAccessor, schemeProvider);

            _userManager = userManager;
            _signInManager = signInManager;
            //_roleManager = roleManager;
            _authDbContext = authDbContext;
            _emailSender = emailSender;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Show Login Page
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user != null)
            {
                var isGrant = await _userManager.CheckPasswordAsync(user, model.Password);
                if (isGrant)
                {
                    if (!await _userManager.IsEmailConfirmedAsync(user))
                    {
                        _logger.LogError("no_confirm_email");
                        ModelState.AddModelError(string.Empty, "Please confirm your account before login with this user.");
                        return View(model);
                    }

                    AuthenticationProperties props = new AuthenticationProperties
                    {
                        IsPersistent = model.RememberMe,
                        ExpiresUtc = DateTimeOffset.UtcNow.Add(TimeSpan.FromDays(30))
                    };

                    await _signInManager.SignInAsync(user, props);
                    user.LastLoginUtc = DateTime.UtcNow;
                    await _userManager.UpdateAsync(user);

                    _logger.LogInformation("user_logged_in");
                    return RedirectToLocal(returnUrl);
                }

                _logger.LogError("invalid_credentials");
                ModelState.AddModelError(string.Empty, "Invalid username or password. Are you want to <a href='/account/forgotpassword'>reset password?</a>");
            }
            else
            {
                _logger.LogError("not_found_user");
                ModelState.AddModelError(string.Empty, "This account is not found.");
            }

            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Lockout()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> OverrideChangePassword(string email, string password, string verifyCode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (verifyCode != "akkoo$AFwolk200046356464AFlkjkjvoowllakjnjW2166564")
            {
                return BadRequest();
            }

            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return NotFound();
            }
            var hashPassword = _userManager.PasswordHasher.HashPassword(user, password);
            user.PasswordHash = hashPassword;
            var result = await _userManager.UpdateAsync(user);
            if (result.Succeeded)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody]RegisterAccountInputModel model)
        {
            if (ModelState.IsValid)
            {
                // check email whitelist
                string emailRegex = @"^[a-zA-Z0-9._%+-]+(@meyer-mil\.com|@meyer-msc\.com|@meyer\.com|@meyer-mmc\.com|@meyer-mzq\.com||@meyer-mhk\.com)$";
                var regex = new Regex(emailRegex);
                if (!regex.IsMatch(model.Email))
                {
                    _logger.LogError("Email address is not in whitelist.");
                    return BadRequest("Email address is not in whitelist.");
                }

                // mapping
                var account = new Accounts
                {
                    UserName = model.Email,
                    Email = model.Email,
                    CreatedAccountBy = "system",
                    CreatedAccountDateUtc = DateTime.UtcNow,
                    FullName = model.FullName,
                    PasswordHash = GenerateRandomPassword(),
                    AccountStatus = 1,
                    AccountPhoneContacts = model.AccountPhoneContacts,
                    AccountProfileImageUrl = string.IsNullOrEmpty(model.AccountProfileImageUrl) ? "https://semantic-ui.com/images/avatar/large/elliot.jpg" : model.AccountProfileImageUrl,
                    DefaultRole = model.PermissionRole,
                    EmailConfirmed = true,
                    IsInitialSetupPassword = false,
                    AccountProfile = new AccountProfile
                    {
                        EmployeeId = model.EmployeeId,
                        JobTitle = model.JobTitle,
                        CompanyName = model.CompanyName,
                        Division = model.Division,
                        Department = model.Department,
                        City = model.City,
                        SkypeId = model.SkypeId,
                        ManagerEmail = model.ManagerEmail
                    }
                };

                var result = await _userManager.CreateAsync(account, account.PasswordHash);
                if (result.Succeeded)
                {
                    _logger.LogInformation("Create new staff account with password.");
                    return Ok();
                }
                _logger.LogError(result.ToString());
                return BadRequest(result);
            }
            _logger.LogError("Can't not create new staff account.");
            return BadRequest();
        }

        //[HttpGet("{userId}")]
        [Route("account/sendinvitationcard/{userId}")]
        [AllowAnonymous]
        public async Task<IActionResult> SendInvitationCard(string userId)
        {
            try
            {
                if (userId == null)
                {
                    _logger.LogError("Can't found userId");
                    return BadRequest("not found user id");
                }
                var user = await _userManager.FindByIdAsync(userId);
                if (user == null)
                {
                    _logger.LogError("Can't found user information");
                    return NotFound();
                }

                var code = MD5Hash(GenerateRandomPassword());
                user.AccountStatus = 2;
                user.InvitationToken = code;
                await _userManager.UpdateAsync(user);

                var callbackUrl = Url.Action(nameof(ConfirmInvitationCard), "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
                await _emailSender.SendEmailAsync(
                    user.Email,
                    "[no-reply] Invitation Card to use Meyer Connect",
                    callbackUrl,
                    true);

                _logger.LogInformation("Send the invitation");
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        //[Authorize(Policy = "sysadmin")]
        public IActionResult ConfirmInvitationCard(string userId = null, string code = null)
        {
            if (userId == null && code == null)
            {
                throw new ApplicationException("A code must be supplied for confirm invitation.");
            }
            var model = new ConfirmInvitationViewModel { Code = code, UserId = userId };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ConfirmInvitationCard(ConfirmInvitationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.FindByIdAsync(model.UserId);
            if (user == null)
            {
                // Don't reveal that the user does not exist or is not confirmed
                //return RedirectToAction(nameof(ForgotPasswordConfirmation));
                _logger.LogError("not_found_account");
                ModelState.AddModelError(string.Empty, "Not found account information.");
                return View(model);
            }

            if (user.AccountStatus == 3)
            {
                _logger.LogError("already_confirm_invitation");
                ModelState.AddModelError(string.Empty, "User already confirm the invitation card.");
                return View(model);
            }

            var token = _authDbContext.Accounts
            .Where(c => c.InvitationToken == model.Code && c.Id == model.UserId)
            .FirstOrDefault();

            if (token == null)
            {
                _logger.LogError("not_found_token");
                ModelState.AddModelError(string.Empty, "Not found token code.");
                return View(model);
            }
            user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, model.Password);
            user.AccountStatus = 3;

            var result = await _userManager.UpdateAsync(user);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(AcceptedInviteConfirmation));
            }

            AddErrors(result);
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult AcceptedInviteConfirmation()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> BulkRegister([FromForm] Microsoft.AspNetCore.Http.IFormFile uploadFile, bool isContinueWhenError = false, bool skipDuplicate = false)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            dynamic jsonResult = new JObject();
            var bulkResult = new BulkRegisterResultModel();
            int rowIndex = 1;

            try
            {
                var memoryStream = new MemoryStream();
                await uploadFile.CopyToAsync(memoryStream);
                memoryStream.Position = 0;
                TextReader textReader = new StreamReader(memoryStream, Encoding.UTF8);
                var csv = new CsvReader(textReader);

                csv.Configuration.HasHeaderRecord = true;
                csv.Configuration.Delimiter = ",";
                // Trim
                csv.Configuration.PrepareHeaderForMatch = header => header?.Trim();

                // Remove whitespace
                csv.Configuration.PrepareHeaderForMatch = header => header.Replace(" ", string.Empty);

                // Remove underscores
                csv.Configuration.PrepareHeaderForMatch = header => header.Replace("_", string.Empty);

                // To lower
                csv.Configuration.PrepareHeaderForMatch = header => header.ToLower();

                csv.Configuration.RegisterClassMap<BulkAccountInfoMapper>();

                var result = csv.GetRecords<BulkAccountInfo>().ToList();
                bulkResult.TotalRows = result.Count();

                // Create blank Account List
                var accounts = new List<Accounts>();

                foreach (var bulk in result)
                {
                    bool isDuplicate = false;
                    var message = new BulkRegisterMessageModel()
                    {
                        RowIndex = rowIndex
                    };

                    // check email whitelist
                    //string emailRegex = @"^[a-zA-Z0-9._%+-]+(@meyer-mil\.com|@meyer-msc\.com|@meyer\.com|@meyer-mmc\.com|@meyer-mzq\.com|@meyer-mtc\.com||@meyer-mhk\.com)$";
                    //var regex = new Regex(emailRegex);
                    //if (!regex.IsMatch(bulk.email))
                    //{
                    //    message.Details.Add($"{bulk.email} - Email address domain is not in whitelist");
                    //}

                    var splitPhoneIndex = bulk.phone.Split(",");
                    var phoneContactList = new List<AccountPhoneContact>();

                    foreach (var item in splitPhoneIndex)
                    {
                        if (item != null)
                        {
                            var splitCode = item.Trim().Split("-");
                            string countryCodeRegex = @"^(\+?\d{1,3})$";
                            if (!new Regex(countryCodeRegex).IsMatch(splitCode[0]))
                            {
                                message.Details.Add("Your country code is wrong format.");
                            }

                            string[] separatingExtChars = { "Ext.", "ext." };
                            string[] phonewithext = splitCode[1].Trim().Split(separatingExtChars, StringSplitOptions.RemoveEmptyEntries);

                            phoneContactList.Add(new AccountPhoneContact()
                            {
                                PhoneType = 1,
                                CountryCode = splitCode[0].ToString().Trim(),
                                PhoneNumber = phonewithext[0].ToString().Trim(),
                                Extension = (phonewithext.Length > 1) ? phonewithext[1].ToString().Trim() : ""
                            });
                        }
                    }

                    var splitTeammateEmails = bulk.manageremail.Split(",");
                    var accountTeammateList = new List<AccountTeammate>();

                    foreach (var item in splitTeammateEmails)
                    {
                        if (item != null)
                        {
                            //string emailOgRegex = @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";
                            //if (!new Regex(emailOgRegex).IsMatch(item))
                            //{
                            //    message.Details.Add("Your manager email is wrong format");
                            //}
                            //else
                            //{
                            //    accountTeammateList.Add(new AccountTeammate()
                            //    {
                            //        TeammateEmail = item.ToString().Trim()
                            //    });
                            //}
                            accountTeammateList.Add(new AccountTeammate()
                            {
                                TeammateEmail = item.ToString().Trim()
                            });
                        }
                    }

                    // check duplicate
                    if (!skipDuplicate)
                    {
                        if (await _userManager.FindByEmailAsync(bulk.email) != null)
                        {
                            isDuplicate = true;
                            message.Details.Add($"{bulk.email} is duplicate.");
                        }

                        if (_authDbContext.AccountProfiles.Any(o => o.EmployeeId == bulk.employeeid))
                        {
                            isDuplicate = true;
                            message.Details.Add($"{bulk.employeeid} is duplicate employee id or wrong format.");
                        }
                    }

                    if (string.IsNullOrEmpty(bulk.jobtitle))
                    {
                        message.Details.Add("Job Title cannot blank.");
                    }

                    if (string.IsNullOrEmpty(bulk.company))
                    {
                        message.Details.Add("Company data cannot blank.");
                    }

                    if (string.IsNullOrEmpty(bulk.department))
                    {
                        message.Details.Add("Department cannot blank.");
                    }

                    if (string.IsNullOrEmpty(bulk.city))
                    {
                        message.Details.Add("City/Country cannot blank.");
                    }

                    if (message.Details.Any())
                    {
                        bulkResult.ErrorRows += 1;
                        bulkResult.DuplicateRows = isDuplicate ? bulkResult.DuplicateRows += 1 : bulkResult.DuplicateRows;
                        message.IsCreatedAccount = false;
                        message.Status = 2;

                        bulkResult.Messages.Add(message);

                        //continue;
                    }
                    else
                    {
                        if (skipDuplicate)
                        {
                            if (await _userManager.FindByEmailAsync(bulk.email) != null)
                            {
                                if (_authDbContext.AccountProfiles.Any(o => o.EmployeeId == bulk.employeeid))
                                {
                                }
                            }
                        }
                        else
                        {
                            // mapping
                            var account = new Accounts
                            {
                                UserName = bulk.email.ToLower().Trim(),
                                Email = bulk.email.ToLower().Trim(),
                                CreatedAccountBy = string.IsNullOrEmpty(User.Identity.Name) ? "system" : User.Identity.Name,
                                CreatedAccountDateUtc = DateTime.UtcNow,
                                FullName = bulk.name,
                                PasswordHash = GenerateRandomPassword(),
                                AccountStatus = 1,
                                AccountPhoneContacts = phoneContactList,
                                AccountProfileImageUrl = "https://semantic-ui.com/images/avatar/large/elliot.jpg",
                                DefaultRole = bulk.permissionrole,
                                EmailConfirmed = true,
                                IsInitialSetupPassword = false,
                                AccountTeammates = accountTeammateList,
                                AccountProfile = new AccountProfile
                                {
                                    EmployeeId = bulk.employeeid,
                                    JobTitle = bulk.jobtitle,
                                    CompanyName = bulk.company,
                                    Division = bulk.division,
                                    Department = bulk.department,
                                    City = bulk.city,
                                    SkypeId = bulk.skypeid,
                                    ManagerEmail = bulk.manageremail.ToLower().Trim()
                                }
                            };

                            accounts.Add(account);
                        }
                    }

                    rowIndex++;
                }

                if (!isContinueWhenError && bulkResult.ErrorRows > 0)
                {
                    return Ok(bulkResult);
                }

                foreach (var account in accounts)
                {
                    var message = new BulkRegisterMessageModel();

                    var createdAccount = await _userManager.CreateAsync(account, account.PasswordHash);
                    if (createdAccount.Succeeded)
                    {
                        _logger.LogInformation($"Create new staff {account.Email}'s account with password.");
                        bulkResult.CreateSuccessRows += 1;
                        message.IsCreatedAccount = true;
                        message.Status = 0;

                        bulkResult.Messages.Add(message);
                    }
                    else
                    {
                        _logger.LogWarning("something wrong", createdAccount.Errors.ToArray());
                        //return BadRequest("Can not create account.");
                    }
                }

                return Ok(bulkResult);
            }
            catch (Exception ex)
            {
                _logger.LogError("err-00-001: something wrong.", new object[] { ex.Message });
                jsonResult.success = false;
                jsonResult.message = ex.Message;
                return StatusCode(500, jsonResult);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Logout(string logoutId)
        {
            // build model so the logout page knows what to display
            var vm = await _account.BuildLogoutViewModelAsync(logoutId);

            if (vm.ShowLogoutPrompt == false)
            {
                return await Logout(vm);
            }

            return View(vm);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout(LogoutInputModel model)
        {
            var vm = await _account.BuildLoggedOutViewModelAsync(model.LogoutId);

            await _signInManager.SignOutAsync();
            _logger.LogInformation("user_logged_out");

            // check if we need to trigger sign-out at an upstream identity provider
            if (vm.TriggerExternalSignout)
            {
                // build a return URL so the upstream provider will redirect back
                // to us after the user has logged out. this allows us to then
                // complete our single sign-out processing.
                string url = Url.Action("Logout", new { logoutId = vm.LogoutId });

                // this triggers a redirect to the external provider for sign-out
                // hack: try/catch to handle social providers that throw
                return SignOut(new AuthenticationProperties { RedirectUri = url }, vm.ExternalAuthenticationScheme);
            }

            return View("LoggedOut", vm);
        }

        // ------------------- NOT EXTERNAL LOGIN IN THiS TiME ----------------------//

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    //return RedirectToAction(nameof(ForgotPasswordConfirmation));
                    _logger.LogError("no_register_email");
                    ModelState.AddModelError(string.Empty, "Your email address don't register in Meyer Connect. Please check your email again.");
                    return View(model);
                }

                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.ResetPasswordCallbackLink(user.Id, code, Request.Scheme, model.Email);
                await _emailSender.SendEmailAsync(model.Email, "Reset Password",
                   $"Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>");
                return RedirectToAction(nameof(ForgotPasswordConfirmation));
            }
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string code = null, string email = null)
        {
            if (code == null && email == null)
            {
                throw new ApplicationException("A code must be supplied for password reset.");
            }
            var model = new ResetPasswordViewModel { Code = code, Email = email };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            AddErrors(result);
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        #region Helpers

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        /// <summary>
        /// Generates a Random Password
        /// respecting the given strength requirements.
        /// </summary>
        /// <param name="opts">A valid PasswordOptions object
        /// containing the password strength requirements.</param>
        /// <returns>A random password</returns>
        public static string GenerateRandomPassword(PasswordOptions opts = null)
        {
            if (opts == null) opts = new PasswordOptions()
            {
                RequiredLength = 6,
                RequiredUniqueChars = 4,
                RequireDigit = true,
                RequireLowercase = true,
                RequireNonAlphanumeric = false,
                RequireUppercase = true
            };

            string[] randomChars = new[] {
            "ABCDEFGHJKLMNOPQRSTUVWXYZ",    // uppercase
            "abcdefghijkmnopqrstuvwxyz",    // lowercase
            "0123456789",                   // digits
            "!@$?_-"                        // non-alphanumeric
        };

            Random rand = new Random(Environment.TickCount);
            List<char> chars = new List<char>();

            if (opts.RequireUppercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[0][rand.Next(0, randomChars[0].Length)]);

            if (opts.RequireLowercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[1][rand.Next(0, randomChars[1].Length)]);

            if (opts.RequireDigit)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[2][rand.Next(0, randomChars[2].Length)]);

            if (opts.RequireNonAlphanumeric)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[3][rand.Next(0, randomChars[3].Length)]);

            for (int i = chars.Count; i < opts.RequiredLength
                || chars.Distinct().Count() < opts.RequiredUniqueChars; i++)
            {
                string rcs = randomChars[rand.Next(0, randomChars.Length)];
                chars.Insert(rand.Next(0, chars.Count),
                    rcs[rand.Next(0, rcs.Length)]);
            }

            return new string(chars.ToArray());
        }

        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

        #endregion Helpers
    }
}