import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Staff } from './staff';
import { StaffList } from './mock-staffs';
import { StaffFilter } from './staff-filter';
import { StaffService } from './../../services/staff.service';
import { Router, ActivatedRoute } from '@angular/router';

declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-staff-directories',
  templateUrl: './staff-directories.component.html',
  styleUrls: ['./staff-directories.component.css']
})
export class StaffDirectoriesComponent implements OnInit {
  staffs: any[];
  departments: any[];
  companies: any[];
  divisions: any[];
  staffFilter: StaffFilter = new StaffFilter();
  useAdminMenu: boolean;
  isContentLoaded = false;

  constructor(
    private staffService: StaffService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.createInitialFilter();
    this.getStaffs();
    $(document).ready(function() {
      $('.ui.dropdown').dropdown();
      $('.ui.image img').visibility({
        type: 'image',
        transition: 'fade in',
        duration: 1000
      });
    });
  }

  createInitialFilter(): void {
    this.staffService.getInitialFilterData().subscribe((data: any) => {
      this.departments = data.departments;
      this.companies = data.companies;
      this.divisions = data.divisions;
    });
  }

  getStaffs(): void {
    this.isContentLoaded = false;
    this.staffService.getStaffs().subscribe((staffs: any) => {
      this.staffs = staffs.items;
      this.isContentLoaded = true;
      console.log(this.authService.getClaims());
    });
  }

  findStaffsByColleague(colleague: string): void {
    this.isContentLoaded = false;
    this.staffs = [];
    this.staffService
      .getStaffsByColleague(colleague)
      .subscribe((staffs: any) => {
        this.staffs = staffs.items;
        this.isContentLoaded = true;
      });
  }

  findStaffsByFilter(staffFilter: StaffFilter): void {
    this.staffService
      .getStaffsByFilter(staffFilter)
      .subscribe((staffs: any) => {
        console.log(staffs.items);
        this.staffs = staffs.items;
      });
  }
}
