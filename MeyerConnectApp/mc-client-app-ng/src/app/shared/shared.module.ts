import { SortableTableDirective } from './sortable-column/sortable-table.directive';
import { ModalDialogModule } from './modal/modal-dialog.module';
import { HeaderComponent } from './header/header.component';
import { ReadmoreComponent } from './readmore/readmore.component';
import { TimeAgoPipe } from './time-ago.pipe';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TruncateModule } from 'ng2-truncate';
import { RouterModule } from '@angular/router';
import { CalendarSimpleModelComponent } from './calendar/calendar-simple-model.component';
import { SortableColumnComponent } from './sortable-column/sortable-column.component';

@NgModule({
  imports: [CommonModule, RouterModule, TruncateModule],
  declarations: [
    TimeAgoPipe,
    ReadmoreComponent,
    HeaderComponent,
    CalendarSimpleModelComponent,
    SortableColumnComponent,
    SortableTableDirective
  ],
  exports: [
    TimeAgoPipe,
    ReadmoreComponent,
    CommonModule,
    FormsModule,
    HeaderComponent,
    CalendarSimpleModelComponent,
    ModalDialogModule,
    SortableColumnComponent,
    SortableTableDirective
  ]
})
export class SharedModule {}
