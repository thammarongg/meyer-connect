import { TruncateModule } from 'ng2-truncate';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-readmore',
  templateUrl: './readmore.component.html',
  styleUrls: ['./readmore.component.css']
})
export class ReadmoreComponent {
  @Input('text') text: string;
  @Input('limit') limit = 40;
  truncating = true;

  setTruncating(isTruncating: boolean): void {
    this.truncating = isTruncating;
  }

  constructor() {}
}
