﻿using Core.Backend.Models.Ltes;
using Core.Backend.Models.Staff;
using Core.Backend.Models.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Repositories.Interface
{
    public interface IAccountRepository
    {
        Task<List<Accounts>> GetAllAccountAsync(int row = 100);
        PagedList<Accounts> GetAllAccount(PagingParams pagingParams);
        Task<List<Accounts>> GetAccountByColleagueAsync(StaffQueryModel model);
        Task<List<Accounts>> GetAccountByFilterAsync(StaffQueryModel model);
        Task<Accounts> GetAccountByIdAsync(string id);
        Task<InitialFilterOutputModel> GetInitialFilterAsync();
        Task<Accounts> GetAccountByEmailAsync(string email);

        IEnumerable<AccountHierarchyModel> FindAccountHierarchy(IEnumerable<Accounts> allAccount, Accounts parentAccount);
        List<Accounts> GetAccountHierarchyById(string userId);

    }
}
