import { OAuthService } from 'angular-oauth2-oidc';
import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { WeatherComponent } from './../weather/weather.component';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  private userProfile: object;
  // private profileImage = 'assets/images/empty-profile-image.png';
  @Input() profileImage = 'assets/images/empty-profile-image.png';
  constructor(
    private oauthService: OAuthService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    $(document).ready(function() {
      $('.page-header').dropdown();
    });
  }

  logout() {
    this.authService.logout();
  }

  private loadUserProfile(): void {
    if (this.oauthService.hasValidAccessToken()) {
      this.oauthService
        .loadUserProfile()
        .then(userProfile => {
          this.userProfile = userProfile;
          this.profileImage = userProfile['picture'];
        })
        .catch(console.log);
    }
  }
}
