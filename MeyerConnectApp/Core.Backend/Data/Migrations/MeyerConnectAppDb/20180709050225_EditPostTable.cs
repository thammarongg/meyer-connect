﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Backend.Data.Migrations.MeyerConnectAppDb
{
    public partial class EditPostTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PublishDate",
                table: "Posts",
                newName: "PublishDateUtc");

            migrationBuilder.RenameColumn(
                name: "FromSubject",
                table: "Posts",
                newName: "Author");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PublishDateUtc",
                table: "Posts",
                newName: "PublishDate");

            migrationBuilder.RenameColumn(
                name: "Author",
                table: "Posts",
                newName: "FromSubject");
        }
    }
}
