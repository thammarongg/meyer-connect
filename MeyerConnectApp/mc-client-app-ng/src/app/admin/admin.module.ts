import { SortableTableDirective } from './../shared/sortable-column/sortable-table.directive';
import { SortableColumnComponent } from './../shared/sortable-column/sortable-column.component';
import { StaffInfoModalComponent } from './manage-staff/staff-info-modal.component';
import { SharedModule } from './../shared/shared.module';
import { WeatherComponent } from './../shared/weather/weather.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminComponent } from './admin/admin.component';
import { ManageStaffComponent } from './manage-staff/manage-staff.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { HeaderComponent } from './../shared/header/header.component';

import { ManageAnnouncementComponent } from './manage-announcement/manage-announcement.component';
import { AdminSidebarComponent } from './../shared/admin-sidebar/admin-sidebar.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule, RouterModule, SharedModule],
  declarations: [
    AdminComponent,
    AdminDashboardComponent,
    ManageStaffComponent,
    ManageAnnouncementComponent,
    AdminSidebarComponent,
    StaffInfoModalComponent,
  ]
})
export class AdminModule {}
