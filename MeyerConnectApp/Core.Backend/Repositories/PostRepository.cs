﻿using Core.Backend.Models;
using Core.Backend.Models.Post;
using Core.Backend.Repositories.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Repositories
{
    public class PostRepository : IPostRepository
    {
        private Func<AppSQLTableDependencyContext> _context;

        public PostRepository(Func<AppSQLTableDependencyContext> context)
        {
            _context = context;
        }

        public async Task<bool> CreatePostAsync(Posts post)
        {
            using (var context = _context.Invoke())
            {
                context.Posts.Add(post);

                return await context.SaveChangesAsync() > 0;
            }
        }

        public async Task<bool> DeletePostAsync(Posts post)
        {
            using (var context = _context.Invoke())
            {
                context.Posts.Remove(post);

                return await context.SaveChangesAsync() > 0;
            }
        }

        public async Task<List<Posts>> GetAllPostAsync(int row = 200)
        {
            using (var context = _context.Invoke())
            {
                var results = await context.Posts
                .Include(t => t.PostAudiences)
                .Include(t => t.PostAttachments)
                .OrderByDescending(c => c.CreatedPostDateUtc)
                .Take(row).ToListAsync();

                return results;
            }
        }

        public async Task<Posts> GetPostByIdAsync(int postId)
        {
            using (var context = _context.Invoke())
            {
                var result = await context.Posts
                .Include(t => t.PostAudiences)
                .Include(t => t.PostAttachments)
                .FirstOrDefaultAsync(c => c.PostId == postId);

                return result;
            }
        }

        public async Task<List<Posts>> GetPostPageAsync(int pageNumber, int pageSize, int typeId)
        {
            using (var context = _context.Invoke())
            {
                if (typeId == 0)
                {
                    var result = context.Posts
                        .Include(t => t.PostAudiences)
                        .Include(t => t.PostAttachments)
                        .OrderByDescending(f => f.PublishDateUtc).Take(pageSize).AsQueryable();
                    return await result.ToListAsync();
                }
                else
                {
                    var result = context.Posts
                        .Include(t => t.PostAudiences)
                        .Include(t => t.PostAttachments)
                        .Where(w => w.PostType == typeId)
                        .OrderByDescending(f => f.PublishDateUtc).Take(pageSize).AsQueryable();
                    return await result.ToListAsync();
                }
            }
        }

        public async Task<List<Posts>> GetPostPageAsync(int pageNumber, int pageSize, int typeId, int statusId)
        {
            using (var context = _context.Invoke())
            {
                if (typeId == 0)
                {
                    var result = context.Posts
                        .Include(t => t.PostAudiences)
                        .Include(t => t.PostAttachments)
                        .Where(w => w.PostStatus == statusId)
                        .OrderByDescending(f => f.PublishDateUtc).Take(pageSize).AsQueryable();
                    return await result.ToListAsync();
                }
                else
                {
                    var result = context.Posts
                        .Include(t => t.PostAudiences)
                        .Include(t => t.PostAttachments)
                        .Where(w => w.PostType == typeId && w.PostStatus == statusId)
                        .OrderByDescending(f => f.PublishDateUtc).Take(pageSize).AsQueryable();
                    return await result.ToListAsync();
                }
            }
        }

        public async Task<bool> UpdatePostAsync(Posts post)
        {
            using (var context = _context.Invoke())
            {
                context.Posts.Update(post);

                return await context.SaveChangesAsync() > 0;
            }
        }
    }
}