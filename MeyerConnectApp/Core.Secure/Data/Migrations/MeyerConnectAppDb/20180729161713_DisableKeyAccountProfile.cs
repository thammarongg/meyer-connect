﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Secure.Data.Migrations.MeyerConnectAppDb
{
    public partial class DisableKeyAccountProfile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_AccountProfile",
                table: "AccountProfile");

            migrationBuilder.DropIndex(
                name: "IX_AccountProfile_UserId",
                table: "AccountProfile");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "AccountProfile",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "EmployeeId",
                table: "AccountProfile",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddPrimaryKey(
                name: "PK_AccountProfile",
                table: "AccountProfile",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_AccountProfile",
                table: "AccountProfile");

            migrationBuilder.AlterColumn<string>(
                name: "EmployeeId",
                table: "AccountProfile",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "AccountProfile",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddPrimaryKey(
                name: "PK_AccountProfile",
                table: "AccountProfile",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountProfile_UserId",
                table: "AccountProfile",
                column: "UserId",
                unique: true,
                filter: "[UserId] IS NOT NULL");
        }
    }
}
