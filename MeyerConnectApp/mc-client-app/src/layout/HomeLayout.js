import React from "react"

const HomeLayout = ({ children, ...rest }) => (
  <div className="layout">
    <div className="layout__top" />
    <div className="layout__children">{children}</div>
    <div className="layout__bottom" />
  </div>
)

export default HomeLayout
