﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Services
{
    public class BaseDataAccess
    {
        protected string ConnectionString { get; set; }

        public BaseDataAccess()
        {
        }

        public BaseDataAccess(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public SqlConnection GetConnection()
        {
            SqlConnection connection = new SqlConnection(this.ConnectionString);
            if (connection.State != ConnectionState.Open)
                connection.Open();
            return connection;
        }

        protected DbCommand GetCommand(DbConnection connection, string commandText, CommandType commandType)
        {
            SqlCommand command = new SqlCommand(commandText, connection as SqlConnection);
            command.CommandType = commandType;
            return command;
        }

        protected SqlParameter GetParameter(string parameter, object value)
        {
            SqlParameter parameterObject = new SqlParameter(parameter, value != null ? value : DBNull.Value);
            parameterObject.Direction = ParameterDirection.Input;
            return parameterObject;
        }

        protected SqlParameter GetParameterOut(string parameter, SqlDbType type, object value = null, ParameterDirection parameterDirection = ParameterDirection.InputOutput)
        {
            SqlParameter parameterObject = new SqlParameter(parameter, type); ;

            if (type == SqlDbType.NVarChar || type == SqlDbType.VarChar || type == SqlDbType.NText || type == SqlDbType.Text)
            {
                parameterObject.Size = -1;
            }

            parameterObject.Direction = parameterDirection;

            if (value != null)
            {
                parameterObject.Value = value;
            }
            else
            {
                parameterObject.Value = DBNull.Value;
            }

            return parameterObject;
        }

        protected int ExecuteNonQuery(string procedureName, List<DbParameter> parameters, CommandType commandType = CommandType.StoredProcedure)
        {
            int returnValue = -1;

            try
            {
                using (SqlConnection connection = this.GetConnection())
                {
                    DbCommand cmd = this.GetCommand(connection, procedureName, commandType);

                    if (parameters != null && parameters.Count > 0)
                    {
                        cmd.Parameters.AddRange(parameters.ToArray());
                    }

                    returnValue = cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                //LogException("Failed to ExecuteNonQuery for " + procedureName, ex, parameters);
                throw ex;
            }

            return returnValue;
        }

        protected object ExecuteScalar(string procedureName, List<DbParameter> parameters)
        {
            object returnValue = null;

            try
            {
                using (DbConnection connection = this.GetConnection())
                {
                    DbCommand cmd = this.GetCommand(connection, procedureName, CommandType.StoredProcedure);

                    if (parameters != null && parameters.Count > 0)
                    {
                        cmd.Parameters.AddRange(parameters.ToArray());
                    }

                    returnValue = cmd.ExecuteScalar();
                }
            }
            catch (Exception ex)
            {
                //LogException("Failed to ExecuteScalar for " + procedureName, ex, parameters);
                throw ex;
            }

            return returnValue;
        }

        protected async Task<object> ExecuteScalarAsync(SqlConnection connection, string procedureName, List<DbParameter> parameters, CommandType commandType = CommandType.StoredProcedure)
        {
            object returnValue = null;

            try
            {
                DbCommand cmd = new SqlCommand(procedureName, connection);
                {
                    cmd.CommandType = commandType;

                    if (parameters != null && parameters.Count > 0)
                    {
                        cmd.Parameters.AddRange(parameters.ToArray());
                    }

                    returnValue = await cmd.ExecuteScalarAsync();
                }
            }
            catch (Exception ex)
            {
                //LogException("Failed to ExecuteScalar for " + procedureName, ex, parameters);
                throw ex;
            }

            return returnValue;
        }

        protected bool BulkCopy(string tableName, DataTable dataTable)
        {
            try
            {
                SqlConnection connection = this.GetConnection();
                {
                    SqlBulkCopy bulk = new SqlBulkCopy(connection,
                        SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.FireTriggers | SqlBulkCopyOptions.UseInternalTransaction,
                        null);
                    bulk.DestinationTableName = tableName;
                    bulk.WriteToServer(dataTable);

                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected DbDataReader GetDataReader(string procedureName, List<DbParameter> parameters, CommandType commandType = CommandType.StoredProcedure)
        {
            DbDataReader ds;

            try
            {
                DbConnection connection = this.GetConnection();
                {
                    DbCommand cmd = this.GetCommand(connection, procedureName, commandType);
                    if (parameters != null && parameters.Count > 0)
                    {
                        cmd.Parameters.AddRange(parameters.ToArray());
                    }

                    ds = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                }
            }
            catch (Exception ex)
            {
                //LogException("Failed to GetDataReader for " + procedureName, ex, parameters);
                throw ex;
            }

            return ds;
        }

        protected IEnumerable<object> GetJson(string procedureName, List<DbParameter> parameters, CommandType commandType = CommandType.StoredProcedure)
        {
            var dynamicObj = new List<dynamic>();

            try
            {
                DbConnection connection = this.GetConnection();
                {
                    DbCommand cmd = this.GetCommand(connection, procedureName, commandType);
                    if (parameters != null && parameters.Count > 0)
                    {
                        cmd.Parameters.AddRange(parameters.ToArray());
                    }

                    using (var dr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (dr.Read())
                        {
                            var dataRow = new ExpandoObject() as IDictionary<string, object>;
                            for (var iField = 0; iField < dr.FieldCount; iField++)
                            {
                                // one can modify the next line to
                                //   if (dataReader.IsDBNull(iFiled))
                                //       dataRow.Add(dataReader.GetName(iFiled), dataReader[iFiled]);
                                // if one want don't fill the property for NULL
                                // returned from the database

                                dataRow.Add(
                                    dr.GetName(iField),
                                    dr.IsDBNull(iField) ? null : dr[iField] // use null instead of {}
                                    );
                            }

                            dynamicObj.Add((ExpandoObject)dataRow);
                        }
                    }
                }

                return dynamicObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected object GetJson2(string procedureName, List<DbParameter> parameters, CommandType commandType = CommandType.StoredProcedure)
        {
            var details = new List<Dictionary<string, object>>();

            try
            {
                DbConnection connection = this.GetConnection();
                {
                    DbCommand cmd = this.GetCommand(connection, procedureName, commandType);
                    if (parameters != null && parameters.Count > 0)
                    {
                        cmd.Parameters.AddRange(parameters.ToArray());
                    }

                    using (var dr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                var dict = new Dictionary<string, object>();

                                for (int i = 0; i < dr.FieldCount; i++)
                                {
                                    dict.Add(dr.GetName(i), dr.IsDBNull(i) ? null : dr.GetValue(i));
                                }

                                details.Add(dict);
                            }
                        }
                    }
                }

                return JsonConvert.SerializeObject(details,
                    Formatting.Indented,
                    new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected async Task<IEnumerable<object>> GetJsonListAsync(SqlConnection connection, string procedureName, List<DbParameter> parameters, CommandType commandType = CommandType.StoredProcedure)
        {
            var dynamicObj = new List<dynamic>();

            try
            {
                DbCommand cmd = new SqlCommand(procedureName, connection);
                {
                    cmd.CommandTimeout = 300;
                    cmd.CommandType = commandType;
                    if (parameters != null && parameters.Count > 0)
                    {
                        cmd.Parameters.AddRange(parameters.ToArray());
                    }

                    using (var dr = await cmd.ExecuteReaderAsync())
                    {
                        while (dr.Read())
                        {
                            var dataRow = new ExpandoObject() as IDictionary<string, object>;
                            for (var iField = 0; iField < dr.FieldCount; iField++)
                            {
                                // one can modify the next line to
                                //   if (dataReader.IsDBNull(iFiled))
                                //       dataRow.Add(dataReader.GetName(iFiled), dataReader[iFiled]);
                                // if one want don't fill the property for NULL
                                // returned from the database

                                dataRow.Add(
                                    dr.GetName(iField),
                                    dr.IsDBNull(iField) ? null : dr[iField] // use null instead of {}
                                    );
                            }

                            dynamicObj.Add((ExpandoObject)dataRow);
                        }
                    }
                }

                return dynamicObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
