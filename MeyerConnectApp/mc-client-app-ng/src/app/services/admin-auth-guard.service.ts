import { StaffService } from './staff.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  CanActivateChild,
  CanLoad,
  NavigationExtras,
  Route
} from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { AuthService } from './auth.service';

@Injectable()
export class AdminAuthGuardService
  implements CanActivate, CanLoad, CanActivateChild {
  constructor(
    private oauthService: OAuthService,
    private router: Router,
    private authService: AuthService,
    private staffService: StaffService
  ) {}

  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {
    return this.canActivate(childRoute, state);
  }
  canLoad(route: Route): boolean | Observable<boolean> | Promise<boolean> {
    const url = `/${route.path}`;
    return this.checkAdminAccount(url);
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {
    const url: string = state.url;
    return this.checkAdminAccount(url);
  }

  checkAdminAccount(url: string): boolean {
    console.log(this.isAdminAccount);
    if (this.isAdminAccount) {
      return true;
    }

    return false;
  }

  get isAdminAccount(): boolean {
    this.staffService.getIsAdminAccount().subscribe((res: boolean) => {
      console.log('res:' + res);
      return true;
    });

    return false;
  }
}
