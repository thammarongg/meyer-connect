﻿using Core.Backend.Models.Staff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Models.Ltes
{
    public class AccountHierarchyModel
    {
        public Accounts accounts { get; set; }
        public IEnumerable<AccountHierarchyModel> accountHierarchies { get; set; }

    }
}
