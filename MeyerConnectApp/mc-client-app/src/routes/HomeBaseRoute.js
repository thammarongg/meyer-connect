import React from "react"
import { Route } from "react-router-dom"
import { HomeLayout } from "../layout"

const HomeBaseRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={matchProps => (
      <HomeLayout>
        <Component {...matchProps} />
      </HomeLayout>
    )}
  />
)

export default HomeBaseRoute
