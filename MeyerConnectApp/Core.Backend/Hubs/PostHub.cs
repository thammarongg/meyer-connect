﻿using Core.Backend.Models.Post;
using Core.Backend.Repositories.Interface;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Hubs
{
    public class PostHub : Hub
    {
        private readonly IPostRepository _repository;
        private readonly ILogger _logger;

        public PostHub(IPostRepository repository, ILogger<PostHub> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public async Task GetAllPost()
        {
            try
            {
                var items = await _repository.GetAllPostAsync();
                if (!items.Any())
                {
                    _logger.LogInformation("not found any post.");
                }

                _logger.LogInformation("return post json data.");

                await Clients.All.SendAsync("GetAllPost", items);
            }
            catch (Exception ex)
            {
                _logger.LogError("err-04-001: something wrong.", new object[] { ex.Message });
                await Clients.All.SendAsync("GetAllPost", new List<Posts>());
            }
        }

    }
}
