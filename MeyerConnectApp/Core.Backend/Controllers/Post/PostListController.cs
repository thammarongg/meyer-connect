﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Backend.Repositories.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Core.Backend.Controllers.Post
{
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/post/v{version:apiVersion}/postList")]
    public class PostListController : Controller
    {
        private readonly ILogger _logger;
        private readonly IPostRepository _repository;
        private readonly IUrlHelper _urlHelper;

        public PostListController(
            ILogger<PostListController> logger,
            IPostRepository repository,
            IUrlHelper urlHelper)
        {
            _logger = logger;
            _repository = repository;
            _urlHelper = urlHelper;
        }

        // GET: api/post/{APIVersion}/PostList
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/PostList/5
        [HttpGet("{id}", Name = "GetById")]
        public string GetById(int id)
        {
            return "value";
        }
        
        // POST: api/PostList
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }
        
        // PUT: api/PostList/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
