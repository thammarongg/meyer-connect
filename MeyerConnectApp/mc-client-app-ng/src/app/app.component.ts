import { environment } from './../environments/environment';
import { authConfig } from './auth.config';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  OAuthService,
  JwksValidationHandler,
  AuthConfig
} from 'angular-oauth2-oidc';

// import { HubConnection } from '@aspnet/signalr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  // private _hubConnection: HubConnection;

  constructor(private oauthService: OAuthService) {
    this.oauthConfiguration();
  }

  ngOnInit(): void {}

  private oauthConfiguration(): void {
    this.oauthService.configure(authConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndTryLogin();

    // Optional
    this.oauthService.setupAutomaticSilentRefresh();
    this.oauthService.events.subscribe(e => {
      console.log('event fired');
    });

    this.oauthService.events
      .filter(e => e.type === 'session_terminated')
      .subscribe(e => {
        console.log('session terminated');
      });

    this.oauthService.events
      .filter(e => e.type === 'token_received')
      .subscribe(e => {
        console.log('loaded user profile');
      });
  }
}
