﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Secure.Services
{
    public class EmailSenderOptions
    {
        public string SmtpHost { get; set; }
        public int Port { get; set; }
        public bool UseDefaultCredentials { get; set; }
        public string SmtpUserName { get; set; }
        public string SmtpPassword { get; set; }
        public bool EnableSSL { get; set; }
    }
}
