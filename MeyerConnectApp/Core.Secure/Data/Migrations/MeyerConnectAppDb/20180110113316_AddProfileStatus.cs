﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Core.Secure.Data.Migrations.MeyerConnectAppDb
{
    public partial class AddProfileStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BusinessTripEmailContact",
                table: "AccountProfile",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BusinessTripLocation",
                table: "AccountProfile",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BusinessTripNote",
                table: "AccountProfile",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BusinessTripPhoneContact",
                table: "AccountProfile",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IntroduceMessage",
                table: "AccountProfile",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProfileStatus",
                table: "AccountProfile",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "VacationDateUtc",
                table: "AccountProfile",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BusinessTripEmailContact",
                table: "AccountProfile");

            migrationBuilder.DropColumn(
                name: "BusinessTripLocation",
                table: "AccountProfile");

            migrationBuilder.DropColumn(
                name: "BusinessTripNote",
                table: "AccountProfile");

            migrationBuilder.DropColumn(
                name: "BusinessTripPhoneContact",
                table: "AccountProfile");

            migrationBuilder.DropColumn(
                name: "IntroduceMessage",
                table: "AccountProfile");

            migrationBuilder.DropColumn(
                name: "ProfileStatus",
                table: "AccountProfile");

            migrationBuilder.DropColumn(
                name: "VacationDateUtc",
                table: "AccountProfile");
        }
    }
}
