﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Services.Cron
{
    public enum DaySeqNumber
    {
        First = 1,
        Second = 2,
        Third = 3,
        Fourth = 4,
    }
}
