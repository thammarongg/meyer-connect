"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _getPrototypeOf = require("babel-runtime/core-js/object/get-prototype-of");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _possibleConstructorReturn2 = require("babel-runtime/helpers/possibleConstructorReturn");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _createClass2 = require("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

var _inherits2 = require("babel-runtime/helpers/inherits");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _semanticUiReact = require("semantic-ui-react");

var _layout = require("../components/layout");

var _layout2 = _interopRequireDefault(_layout);

var _index = require("../styles/index.scss");

var _index2 = _interopRequireDefault(_index);

var _store = require("../store");

var _mobxReact = require("mobx-react");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = "C:\\Projects\\BitBucket\\meyer-connect\\MeyerConnectApp\\client-app-nextjs\\pages\\index.js?entry";


var Index = function (_React$Component) {
  (0, _inherits3.default)(Index, _React$Component);

  (0, _createClass3.default)(Index, null, [{
    key: "getInitialProps",
    value: function getInitialProps(_ref) {
      var req = _ref.req;

      var isServer = !!req;
      var store = (0, _store.initStore)(isServer);
      return { lastUpdate: store.lastUpdate, isServer: isServer };
    }
  }]);

  function Index(props) {
    (0, _classCallCheck3.default)(this, Index);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Index.__proto__ || (0, _getPrototypeOf2.default)(Index)).call(this, props));

    _this.store = (0, _store.initStore)(props.isServer, props.lastUpdate);
    return _this;
  }

  (0, _createClass3.default)(Index, [{
    key: "render",
    value: function render() {
      return _react2.default.createElement(_mobxReact.Provider, { store: this.store, __source: {
          fileName: _jsxFileName,
          lineNumber: 22
        }
      }, _react2.default.createElement(_layout2.default, { title: "Meyer Connect App", __source: {
          fileName: _jsxFileName,
          lineNumber: 23
        }
      }, _react2.default.createElement("style", { dangerouslySetInnerHTML: { __html: _index2.default }, __source: {
          fileName: _jsxFileName,
          lineNumber: 24
        }
      }), _react2.default.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 25
        }
      }, "ciao")));
    }
  }]);

  return Index;
}(_react2.default.Component);

exports.default = Index;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhZ2VzXFxpbmRleC5qcyJdLCJuYW1lcyI6WyJSZWFjdCIsIk1vZGFsIiwiSGVhZGVyIiwiQnV0dG9uIiwiTGlzdCIsIkxheW91dCIsInN0eWxlc2hlZXQiLCJpbml0U3RvcmUiLCJQcm92aWRlciIsIkluZGV4IiwicmVxIiwiaXNTZXJ2ZXIiLCJzdG9yZSIsImxhc3RVcGRhdGUiLCJwcm9wcyIsIl9faHRtbCIsIkNvbXBvbmVudCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsQUFBTzs7OztBQUNQLEFBQVMsQUFBTyxBQUFRLEFBQVE7O0FBQ2hDLEFBQU8sQUFBWTs7OztBQUNuQixBQUFPLEFBQWdCOzs7O0FBQ3ZCLEFBQVMsQUFBaUI7O0FBQzFCLEFBQVM7Ozs7Ozs7SUFFWSxBOzs7OzswQ0FDYTtVQUFQLEFBQU8sV0FBUCxBQUFPLEFBQzlCOztVQUFNLFdBQVcsQ0FBQyxDQUFsQixBQUFtQixBQUNuQjtVQUFNLFFBQVEsc0JBQWQsQUFBYyxBQUFVLEFBQ3hCO2FBQU8sRUFBRSxZQUFZLE1BQWQsQUFBb0IsWUFBWSxVQUF2QyxBQUFPLEFBQ1I7QUFFRDs7O2lCQUFBLEFBQVksT0FBTzt3Q0FBQTs7b0lBQUEsQUFDWCxBQUNOOztVQUFBLEFBQUssUUFBUSxzQkFBVSxNQUFWLEFBQWdCLFVBQVUsTUFGdEIsQUFFakIsQUFBYSxBQUFnQztXQUM5Qzs7Ozs7NkJBRVEsQUFDUDs2QkFDRSxBQUFDLHFDQUFTLE9BQU8sS0FBakIsQUFBc0I7b0JBQXRCO3NCQUFBLEFBQ0U7QUFERjtPQUFBLGtCQUNFLEFBQUMsa0NBQU8sT0FBUixBQUFjO29CQUFkO3NCQUFBLEFBQ0U7QUFERjtrREFDUyx5QkFBeUIsRUFBaEMsQUFBZ0MsQUFBRSxBQUFRO29CQUExQztzQkFERixBQUNFLEFBQ0E7QUFEQTswQkFDQSxjQUFBOztvQkFBQTtzQkFBQTtBQUFBO0FBQUEsU0FKTixBQUNFLEFBQ0UsQUFFRSxBQUlQOzs7OztFQXJCZ0MsZ0JBQU0sQTs7a0JBQXBCLEEiLCJmaWxlIjoiaW5kZXguanM/ZW50cnkiLCJzb3VyY2VSb290IjoiQzovUHJvamVjdHMvQml0QnVja2V0L21leWVyLWNvbm5lY3QvTWV5ZXJDb25uZWN0QXBwL2NsaWVudC1hcHAtbmV4dGpzIn0=