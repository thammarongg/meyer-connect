﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Models.Post
{
    public class Posts
    {
       

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PostId { get; set; }

        [Required]
        public string TitleName { get; set; }

        public string Description { get; set; }

        [Required]
        public string Author { get; set; }

        [Required]
        public string Contents { get; set; }

        [Required]
        public DateTime PublishDateUtc { get; set; } = DateTime.UtcNow;

        [Required]
        public int PostType { get; set; }  // 1 = news, 2 = annoucement

        [Required]
        public bool IsApproved { get; set; }

        [Required]
        public int PostStatus { get; set; } // 1: Draft, 2: Schedule, 3: Publish

        [Required]
        public string CreatedPostBy { get; set; }

        [Required]
        [DefaultValue("GETUTCDATE()")]
        public DateTime CreatedPostDateUtc { get; set; } = DateTime.UtcNow;


        [Required]
        public string LastModifiedBy { get; set; }

        [Required]
        [DefaultValue("GETUTCDATE()")]
        public DateTime LastModifiedDateUtc { get; set; }

        [Required]
        public int Revise { get; set; }

        public virtual ICollection<PostAudiences> PostAudiences { get; set; }
        public virtual ICollection<PostAttachments> PostAttachments { get; set; }


    }
}
