﻿using Core.Backend.Models.Post;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Repositories.Interface
{
    public interface IPostRepository
    {
        Task<List<Posts>> GetAllPostAsync(int row = 200);
        Task<Posts> GetPostByIdAsync(int postId);

        Task<bool> CreatePostAsync(Posts post);
        Task<bool> UpdatePostAsync(Posts post);
        Task<bool> DeletePostAsync(Posts post);

        Task<List<Posts>> GetPostPageAsync(int pageNumber, int pageSize, int typeId);
        Task<List<Posts>> GetPostPageAsync(int pageNumber, int pageSize, int typeId, int statusId);
    }
}
