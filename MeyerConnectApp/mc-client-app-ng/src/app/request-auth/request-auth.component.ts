import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-request-auth',
  templateUrl: './request-auth.component.html',
  styleUrls: ['./request-auth.component.css']
})
export class RequestAuthComponent implements OnInit {
  constructor(private oauthService: OAuthService, private router: Router) {}

  ngOnInit() {
    if (!this.oauthService.hasValidAccessToken()) {
      this.oauthService.initImplicitFlow();
    } else {
      this.router.navigate(['/']);
    }
  }
}
