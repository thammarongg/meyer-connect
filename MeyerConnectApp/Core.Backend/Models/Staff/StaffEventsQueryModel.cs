﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Models.Staff
{
    public class StaffEventsQueryModel
    {
        public string TweetStatus { get; set; }
        [Required]
        public string UserId { get; set; }
    }
}
