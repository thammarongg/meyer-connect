﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Models.Post
{
    public class PostAudiences
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Seq { get; set; }

        [Required]
        public bool IsAllGroup { get; set; }

        public string StaffGroup { get; set; }

        public string StaffTeam { get; set; }

        public string StaffLevel { get; set; }

        public int PostId { get; set; }

        [NotMapped]
        public virtual Posts Posts { get; set; }

    }
}
