import { Component, OnInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  profileImage = localStorage.getItem('mc.profileImage')
    ? localStorage.getItem('mc.profileImage')
    : 'assets/images/empty-profile-image.png';
  constructor(private oauthService: OAuthService) {}

  ngOnInit(): void {
    this.oauthService
      .loadUserProfile()
      .then(userProfile => {
        if (userProfile) {
          localStorage.setItem('mc.profileImage', userProfile['picture']);
          this.profileImage = userProfile['picture'];
        }
      })
      .catch(console.log);
  }
}
