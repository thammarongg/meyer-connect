import { Image } from "semantic-ui-react"

const anonymousProfileImage =
  "//semantic-ui.com/images/wireframe/square-image.png"

export default () => ( <Image src={anonymousProfileImage} size="mini" circular /> )
