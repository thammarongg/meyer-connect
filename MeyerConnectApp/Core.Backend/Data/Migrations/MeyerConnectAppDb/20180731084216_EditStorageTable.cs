﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Backend.Data.Migrations.MeyerConnectAppDb
{
    public partial class EditStorageTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.RenameColumn(
                name: "FileName",
                table: "Storages",
                newName: "Visibility");

            migrationBuilder.RenameColumn(
                name: "CreatedDateUtc",
                table: "Storages",
                newName: "ModifiedDateUtc");

            migrationBuilder.AddColumn<string>(
                name: "ContentLinkUrl",
                table: "Storages",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Storages",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FileTitle",
                table: "Storages",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<bool>(
                name: "Trashed",
                table: "Storages",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ContentLinkUrl",
                table: "Storages");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Storages");

            migrationBuilder.DropColumn(
                name: "FileTitle",
                table: "Storages");

            migrationBuilder.DropColumn(
                name: "Trashed",
                table: "Storages");

            migrationBuilder.RenameColumn(
                name: "Visibility",
                table: "Storages",
                newName: "FileName");

            migrationBuilder.RenameColumn(
                name: "ModifiedDateUtc",
                table: "Storages",
                newName: "CreatedDateUtc");

        }
    }
}
