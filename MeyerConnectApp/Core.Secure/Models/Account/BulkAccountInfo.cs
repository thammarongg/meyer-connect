﻿using CsvHelper.Configuration;
using System.Collections.Generic;

namespace Core.Secure.Models.Account
{
    public class BulkAccountInfo
    {
        public string employeeid { get; set; }
        public string jobtitle { get; set; }
        public string company { get; set; }
        public string division { get; set; }
        public string department { get; set; }
        public string city { get; set; }
        public string email { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string skypeid { get; set; }
        public string manageremail { get; set; }
        public string permissionrole { get; set; }
    }

    public class BulkRegisterResultModel
    {
        public int TotalRows { get; set; } = 0;
        public int CreateSuccessRows { get; set; } = 0;
        public int DuplicateRows { get; set; } = 0;
        public int ErrorRows { get; set; } = 0;
        public List<BulkRegisterMessageModel> Messages { get; set; } = new List<BulkRegisterMessageModel>();
    }

    public class BulkRegisterMessageModel
    {
        public int RowIndex { get; set; } = 0;
        public bool IsCreatedAccount { get; set; } = false;
        public int Status { get; set; } = 0; // 0: success, 1: duplicate, 2: error
        public List<string> Details { get; set; } = new List<string>();
    }

    public class BulkAccountInfoMapper : ClassMap<BulkAccountInfo>
    {
        public BulkAccountInfoMapper()
        {
            Map(m => m.employeeid).Name("employeeid");
            Map(m => m.jobtitle).Name("jobtitle");
            Map(m => m.company).Name("company");
            Map(m => m.division).Name("division");
            Map(m => m.department).Name("department");
            Map(m => m.city).Name("city");
            Map(m => m.email).Name("email");
            Map(m => m.name).Name("name");
            Map(m => m.phone).Name("phone");
            Map(m => m.skypeid).Name("skypeid");
            Map(m => m.manageremail).Name("manageremail");
            Map(m => m.permissionrole).Name("permissionrole");
        }
    }
}