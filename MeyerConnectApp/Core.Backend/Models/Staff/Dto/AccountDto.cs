﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Models.Staff.Dto
{
    public class AccountDto
    {
        public int AccountStatus { get; set; }
        public string Id { get; set; }
        public string UserName { get; set; }
    }
}
