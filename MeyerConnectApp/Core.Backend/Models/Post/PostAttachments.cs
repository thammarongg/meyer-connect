﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Models.Post
{
    public class PostAttachments
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Seq { get; set; }
        [Required]
        public string AttachmentName { get; set; }
        [Required]
        public string AttachmentPath { get; set; }
        [Required]
        public bool IsVisible { get; set; }
        public int PostId { get; set; }

        [NotMapped]
        public virtual Posts Posts { get; set; }
    }
}
