export class Weather {
    temperature: number;
    conditionCode: number;
    conditionText: string;
    unit: string;
    city: string;
    country: string;
    region: string;
}
