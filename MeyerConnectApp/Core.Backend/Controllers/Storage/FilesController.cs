﻿using Core.Backend.Models.Storage;
using Core.Backend.Repositories.Interface;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Core.Backend.Controllers.Storage
{
    [ApiVersion("1.0")]
    [Route("api/storage/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class FilesController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly IUrlHelper _urlHelper;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IStorageRepository _storageRepository;

        public FilesController(
            ILogger<FilesController> logger,
            IUrlHelper urlHelper,
            IHostingEnvironment hostingEnvironment,
            IStorageRepository storageRepository
            )
        {
            _logger = logger;
            _urlHelper = urlHelper;
            _hostingEnvironment = hostingEnvironment;
            _storageRepository = storageRepository;
        }

        // GET: api/storage/{ApiVersion}/files
        [HttpGet(Name = "GetFileList")]
        public IActionResult GetFileList()
        {
            dynamic jsonResult = new JObject();

            try
            {
                var results = _storageRepository.GetAllFile();
                if (!results.Any())
                {
                    _logger.LogWarning("Storage:File -> not found any file");
                    return NotFound("not found any file");
                }

                _logger.LogInformation("Storage:File -> return json data");
                jsonResult.items = JToken.FromObject(results);
                return Ok(jsonResult);
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Storage:File -> error found -> {ex.Message}");
                return StatusCode(500, ex.Message);
            }
        }

        // GET: api/storage/{ApiVersion}/files/{fileId}
        [HttpGet("{fileId}", Name = "GetFileById")]
        public IActionResult GetFileById(string fileId)
        {
            dynamic jsonResult = new JObject();

            try
            {
                var result = _storageRepository.GetFileById(fileId);
                if (result == null)
                {
                    _logger.LogWarning("Storage:File -> file not found");
                    return NotFound("file not found");
                }

                _logger.LogInformation("Storage:File -> return json data");
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Storage:File -> error -> {ex.Message}");
                return StatusCode(500, ex.Message);
            }
        }

        // POST: api/storage/{ApiVersion}/files
        [HttpPost(Name = "CreateFileToStorage"), DisableRequestSizeLimit]
        public async Task<IActionResult> CreateFileToStorage(string uploadType = "media")
        {
            try
            {
                if (uploadType != "media")
                {
                    _logger.LogWarning("Storage:File -> invalid upload type");
                    return NotFound("invalid upload type");
                }

                foreach (var file in Request.Form.Files)
                {
                    var mimeType = file.ContentType;
                    string folderName = "Storages\\Data";
                    string webRootPath = _hostingEnvironment.WebRootPath;
                    string storagePath = Path.Combine(webRootPath, folderName);

                    if (!Directory.Exists(storagePath))
                    {
                        Directory.CreateDirectory(storagePath);
                    }

                    if (file.Length > 0)
                    {
                        string fileId = Guid.NewGuid().ToString("N");
                        string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                        //string fullPath = Path.Combine(
                        //    storagePath,
                        //    string.Format("{0}{1}", Guid.NewGuid().ToString(), Path.GetExtension(fileName))
                        //    );
                        string fullPath = Path.Combine(
                            storagePath,
                            fileName
                            );
                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            await file.CopyToAsync(stream);
                        }

                        var storage = new Storages()
                        {
                            FileId = fileId,
                            FileTitle = fileName,
                            MimeType = mimeType,
                            ContentLinkUrl = string.Format("/api/storage/{0}?id={1}", "uc", fileId),
                            Trashed = false,
                            Visibility = "anyoneWithLink",
                            CreatedBy = "Administrator"
                        };

                        var result = await _storageRepository.CreateFileDataAsync(storage);
                        if (!result)
                        {
                            throw new Exception("can not create file index data");
                        }
                    }
                    else
                    {
                        _logger.LogWarning("Storage:File -> not found any upload file");
                        return NotFound();
                    }
                }

                _logger.LogInformation("Storage:File -> uploaded");
                return StatusCode(201);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Storage:File -> error -> {ex.Message}");
                return StatusCode(500, ex.Message);
            }
        }

        // PATCH: api/storage/{ApiVersion}/files/{fileId}
        [HttpPatch(Name = "UpdateFileWithId"), DisableRequestSizeLimit]
        public async Task<IActionResult> UpdateFileWithId(string fileId, string uploadType = "media")
        {
            try
            {
                if (uploadType != "media")
                {
                    _logger.LogWarning("Storage:File -> invalid upload type");
                    return NotFound("invalid upload type");
                }

                foreach (var file in Request.Form.Files)
                {
                    var mimeType = file.ContentType;
                    string folderName = "Storages\\Data";
                    string webRootPath = _hostingEnvironment.WebRootPath;
                    string storagePath = Path.Combine(webRootPath, folderName);

                    if (!Directory.Exists(storagePath))
                    {
                        Directory.CreateDirectory(storagePath);
                    }

                    if (file.Length > 0)
                    {
                        string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                        //string fullPath = Path.Combine(
                        //    storagePath,
                        //    string.Format("{0}{1}", Guid.NewGuid().ToString(), Path.GetExtension(fileName))
                        //    );
                        string fullPath = Path.Combine(
                            storagePath,
                            fileName
                            );
                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            await file.CopyToAsync(stream);
                        }

                        var storage = _storageRepository.GetFileById(fileId);

                        storage.FileId = fileId;
                        storage.FileTitle = fileName;
                        storage.MimeType = mimeType;
                        storage.Visibility = "anyoneWithLink";
                        storage.CreatedBy = "Administrator";

                        var result = await _storageRepository.UpdateFileDataAsync(storage);
                        if (!result)
                        {
                            throw new Exception("can not create file index data");
                        }
                    }
                    else
                    {
                        _logger.LogWarning("Storage:File -> not found any upload file");
                        return NotFound();
                    }
                }

                _logger.LogInformation("Storage:File -> uploaded");
                return StatusCode(201);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Storage:File -> error -> {ex.Message}");
                return StatusCode(500, ex.Message);
            }
        }

        // DELETE: api/storage/{ApiVersion}/files/{fileId}
        [HttpDelete("{fileId}", Name = "DeleteFileById")]
        public async Task<IActionResult> DeleteFileById(string fileId)
        {
            try
            {
                var storage = _storageRepository.GetFileById(fileId);
                if (storage == null)
                {
                    _logger.LogWarning("Storage:File -> file not found");
                    return NotFound("file not found");
                }

                var result = await _storageRepository.DeleteFileDataAsync(storage);
                if (result)
                {
                    _logger.LogInformation("Storage:File -> deleted");
                    return StatusCode(204);
                }
                else
                {
                    throw new Exception("can not delete that file");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Storage:File -> error -> {ex.Message}");
                return StatusCode(500, ex.Message);
            }
        }
    }
}