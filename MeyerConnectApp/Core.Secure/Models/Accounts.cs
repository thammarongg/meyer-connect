﻿using Core.Secure.Models.Account;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Secure.Models
{
    public class Accounts : IdentityUser
    {
        // 1 = new register; 2 = wait invite response, 3 = active, 4 = disable, 5 = suspend
        [Required]
        public int AccountStatus { get; set; }
        [Required]
        public string CreatedAccountBy { get; set; }
        [Required]
        public DateTime CreatedAccountDateUtc { get; set; }
        [Required]
        public string FullName { get; set; }
        public DateTime LastLoginUtc { get; set; }
        public string AccountProfileImageUrl { get; set; }
        [Required]
        public string DefaultRole { get; set; }
        [Required]
        public bool IsInitialSetupPassword { get; set; }
        public string InvitationToken { get; set; }

        public AccountProfile AccountProfile { get; set; }

        public virtual ICollection<AccountPhoneContact> AccountPhoneContacts { get; set; }

        public virtual ICollection<AccountTeammate> AccountTeammates { get; set; }
    }
}
