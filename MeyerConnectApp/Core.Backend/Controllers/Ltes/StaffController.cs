﻿using Core.Backend.Models;
using Core.Backend.Repositories;
using Core.Backend.Repositories.Interface;
using Core.Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;

namespace Core.Backend.Controllers.Ltes
{
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/Ltes/Staff")]
    public class StaffController : Controller
    {
        private readonly AppDbContext _context;
        private readonly ILogger _logger;
        private readonly IEmailSender _emailSender;
        private readonly IStaffsRepository _repo;

        public StaffController(
            AppDbContext context,
            ILogger<StaffController> logger,
            IEmailSender emailSender,
            IStaffsRepository repo)
        {
            _context = context;
            _logger = logger;
            _emailSender = emailSender;
            _repo = repo;
        }

        [HttpGet]
        [Route("relational")]
        [Authorize]
        public IActionResult GetStaffRelational(string userId)
        {
            dynamic jsonResult = new JObject();

            try
            {
                var items = _repo.GetAccountHierarchyById(userId);
                if (!items.Any())
                {
                    _logger.LogWarning("warn-03-001: not found data.");
                    return NotFound("not found data.");
                }

                _logger.LogInformation("return json data");
                return Ok(items);
            }
            catch (Exception ex)
            {
                _logger.LogError("err-03-001: something wrong.", new object[] { ex.Message });
                jsonResult.message = ex.Message;
                return StatusCode(500, jsonResult);
            }
        }

        //[HttpGet]
        //[Route("relational")]
        //public async Task<IActionResult> GetStaffRelational(string userId)
        //{
        //    dynamic jsonResult = new JObject();

        //    try
        //    {
        //        var items = await _repo.GetAccountRelationalById(userId);
        //        if (!items.Any())
        //        {
        //            _logger.LogWarning("warn-03-001: not found data.");
        //            return NotFound("not found data.");
        //        }

        //        _logger.LogInformation("return json data");
        //        return Ok(items);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError("err-03-001: something wrong.", new object[] { ex.Message });
        //        jsonResult.message = ex.Message;
        //        return StatusCode(500, jsonResult);
        //    }
        //}
    }
}