export class StaffPhone {
    PhoneType: number;
    CountryCode: string;
    PhoneNumber: string;
}
