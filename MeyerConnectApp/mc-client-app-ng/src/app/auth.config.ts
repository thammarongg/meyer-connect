import { environment } from './../environments/environment';
import { AuthConfig } from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {
  issuer: `${environment.secureUrl}`,
  redirectUri: `${window.location.origin}/auth-callback`,
  clientId: '8611c27e76b358c742a4ff60ac909d07@meyer-connect.com',
  postLogoutRedirectUri: `${window.location.origin}/`,
  responseType: 'id_token token',
  scope: 'openid profile role offline_access mcPermissionRoles',
  silentRefreshRedirectUri: `${
    window.location.origin
  }/slient-token-refresh.html`,
  userinfoEndpoint: `${environment.secureUrl}/connect/userinfo`,
  showDebugInformation: true,
  sessionChecksEnabled: true
};
