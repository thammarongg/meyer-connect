﻿using Core.Backend.Models;
using Core.Backend.Models.Staff;
using Core.Backend.Repositories;
using Core.Backend.Repositories.Interface;
using Core.Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Threading.Tasks;

namespace Core.Backend.Controllers.Staff
{
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/Staff/Events")]
    [Authorize]
    public class EventsController : Controller
    {
        private readonly AppDbContext _context;
        private readonly ILogger _logger;
        private readonly IEmailSender _emailSender;
        private readonly IStaffsRepository _repo;

        public EventsController(
            AppDbContext context,
            ILogger<EventsController> logger,
            IEmailSender emailSender,
            IStaffsRepository repo)
        {
            _context = context;
            _logger = logger;
            _emailSender = emailSender;
            _repo = repo;
        }

        // POST: api/{ApiVersion}/Staff/Events/Status
        [HttpPost]
        [Route("Status")]
        public async Task<IActionResult> UpdateStaffStatus([FromBody] StaffEventsQueryModel model)
        {
            dynamic jsonResult = new JObject();

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                var dict = await _repo.UpdateStaffTweetStatus(model);

                if (!(bool)dict["HasUserProfile"])
                {
                    _logger.LogWarning("not found user.");
                    return NotFound();
                }

                if (!(bool)dict["HasUpdated"])
                {
                    _logger.LogWarning("can not update tweet status.");
                    return BadRequest();
                }

                jsonResult.message = "updated";

                _logger.LogInformation("updated tweet staff message");
                return Ok(jsonResult);
            }
            catch (Exception ex)
            {
                _logger.LogError("err-02-001: something wrong.", new object[] { ex.Message });
                jsonResult.message = ex.Message;
                return StatusCode(500, jsonResult);
            }
        }
    }
}