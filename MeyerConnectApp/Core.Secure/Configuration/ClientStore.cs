﻿using IdentityModel;
using IdentityServer4.Models;
using System.Collections.Generic;
using static IdentityServer4.IdentityServerConstants;

namespace Core.Secure.Configuration
{
    public class ClientStore
    {
        public static IEnumerable<IdentityResource> GetIdentityResource()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email(),
                new IdentityResource("custom.profile",new[]
                {
                    JwtClaimTypes.Subject,
                    JwtClaimTypes.Name,
                    JwtClaimTypes.PreferredUserName,
                    JwtClaimTypes.Profile,
                    JwtClaimTypes.Picture,
                    JwtClaimTypes.Role
                }),
                new IdentityResource("mcPermissionScopes", new []{
                    JwtClaimTypes.Role,
                    "sysadmin",
                    "opadmin",
                    "staff",
                    "mcPermissionRoles",
                    "mcPermissionRoles.sysadmin",
                    "mcPermissionRoles.opadmin",
                    "mcPermissionRoles.staff"
                }),
                new IdentityResource("meyerConnectScopes", new[]
                {
                    "role",
                    "sysadmin",
                    "moderator",
                    "staff",
                    "meyerConnectRoles",
                    "meyerConnectRoles.sysadmin",
                    "meyerConnectRoles.moderator",
                    "meyerConnectRoles.staff",
                    "wikiRoles",
                    "wikiroles.admin",
                    "wikiroles.user"
                })
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                //new ApiResource
                //{
                //    Name = "mcPermissionRoles",
                //    ApiSecrets =
                //    {
                //        new Secret("MeyerConnectSecret".Sha256())
                //    },
                //    UserClaims =
                //    {
                //        JwtClaimTypes.Name,
                //        JwtClaimTypes.Email,
                //        JwtClaimTypes.Role,
                //        "sysadmin",
                //        "opadmin",
                //        "staff",
                //        "mcPermissionRoles",
                //        "mcPermissionRoles.sysadmin",
                //        "mcPermissionRoles.opadmin",
                //        "mcPermissionRoles.staff"
                //    },
                //    Scopes =
                //    {
                //        new Scope()
                //        {
                //            Name = "mcPermissionScopes",
                //            DisplayName = "Scope for the mcPermissionRoles ApiResource"
                //        }
                //    }
                //},
                new ApiResource("meyerConnectRoles")
                {
                    Scopes =
                    {
                        new Scope
                        {
                            Name = "dataeventrecords",
                            DisplayName = "Scope for the meyerconnectRoles ApiResource"
                        }
                    },
                    UserClaims = {
                        "role",
                        "sysadmin",
                        "moderator",
                        "staff",
                        "meyerConnectRoles",
                        "meyerConnectRoles.sysadmin",
                        "meyerConnectRoles.moderator",
                        "meyerConnectRoles.staff",
                        "wikiRoles",
                        "wikiroles.admin",
                        "wikiroles.user"
                    }
                },

                new ApiResource("mcPermissionRoles","Role in MC",
                new[] { JwtClaimTypes.Name,
                        JwtClaimTypes.Email,
                        JwtClaimTypes.Role,
                        "sysadmin",
                        "opadmin",
                        "staff",
                        "mcPermissionRoles",
                        "mcPermissionRoles.sysadmin",
                        "mcPermissionRoles.opadmin",
                        "mcPermissionRoles.staff" }),

                new ApiResource("api1",
                "my api",
                new[] { JwtClaimTypes.Name, JwtClaimTypes.Role, "office" }), // Claims to be included in access token)
                new ApiResource("role",
                "Meyer Connect Role",
                new[]
                {
                    JwtClaimTypes.Subject,
                    JwtClaimTypes.Name,
                    JwtClaimTypes.PreferredUserName,
                    JwtClaimTypes.Profile,
                    JwtClaimTypes.Picture,
                    JwtClaimTypes.Role
                })
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                new Client
                {
                    Enabled = true,
                    ClientId = "meyerconnectapp",
                    ClientName = "Meyer Connect Application",
                    RequireConsent = false,
                    AllowAccessTokensViaBrowser = true,
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials,
                    ClientSecrets = new List<Secret>()
                    {
                        new Secret("e12f21fabdb979c89a8784e98477b542".Sha256())
                    },
                    RedirectUris = new List<string>
                    {
                        "http://localhost:60001/signin-oidc",
                        "http://localhost:26007/signin-oidc",
                        "https://api.meyer-connect.com/signin-oidc",
                        "https://wiki.meyer-connect.com/oauthcallback"
                    },
                    PostLogoutRedirectUris = new List<string>
                    {
                        "http://localhost:60001/signout-callback-oidc",
                        "http://localhost:26007/signout-callback-oidc",
                        "https://api.meyer-connect.com/signout-callback-oidc",
                        "https://wiki.meyer-connect.com/signout-callback-oidc"
                    },
                    AllowedCorsOrigins = new List<string>
                    {
                        "http://localhost:60001",
                        "http://localhost:26007",
                        "https://api.meyer-connect.com",
                        "https://www.meyer-connect.com",
                        "https://wiki.meyer-connect.com",
                        "http://localhost:4200",
                        "http://localhost:4300"
                    },
                    AllowedScopes = new List<string>
                    {
                        StandardScopes.OpenId,
                        StandardScopes.Profile,
                        StandardScopes.OfflineAccess,
                        "api1",
                        "mcPermissionRoles",
                        //"mcPermissionScopes",
                        StandardScopes.Email,
                        "role"
                    },
                    AccessTokenLifetime = 3600,
                    AllowOfflineAccess = true
                },
                new Client
                {
                    Enabled = true,
                    ClientId = "ab28052d553b86286bc411a9870cfaaf@meyer-connect.com",
                    RequireConsent = false,
                    AllowAccessTokensViaBrowser = true,
                    AllowedGrantTypes = GrantTypes.CodeAndClientCredentials,
                    ClientSecrets = new List<Secret>()
                    {
                        new Secret("6e1d02bfa8f5f81ab95a734aba211ceb".Sha256())
                    },
                    RedirectUris = new List<string>
                    {
                        "http://localhost:60001/signin-oidc",
                        "http://localhost:26007/signin-oidc",
                        "https://api.meyer-connect.com/signin-oidc",
                        "https://wiki.meyer-connect.com/oauthcallback"
                    },
                    PostLogoutRedirectUris = new List<string>
                    {
                        "http://localhost:60001/signout-callback-oidc",
                        "http://localhost:26007/signout-callback-oidc",
                        "https://api.meyer-connect.com/signout-callback-oidc",
                        "https://wiki.meyer-connect.com"
                    },
                    AllowedCorsOrigins = new List<string>
                    {
                        "http://localhost:60001",
                        "http://localhost:26007",
                        "https://api.meyer-connect.com",
                        "https://www.meyer-connect.com",
                        "https://wiki.meyer-connect.com"
                    },
                    AllowedScopes = new List<string>
                    {
                        StandardScopes.OpenId,
                        StandardScopes.Profile,
                        StandardScopes.OfflineAccess,
                        "api1",
                        "mcPermissionRoles",
                        //"mcPermissionScopes",
                        StandardScopes.Email,
                        "role"
                    },
                    AccessTokenLifetime = 3600,
                    AllowOfflineAccess = true
                },
                new Client
                {
                    Enabled = true,
                    ClientId = "8611c27e76b358c742a4ff60ac909d07@meyer-connect.com",
                    RequireConsent = false,
                    AllowAccessTokensViaBrowser = true,
                    AllowedGrantTypes = GrantTypes.Implicit,
                    ClientSecrets = new List<Secret>()
                    {
                        new Secret("6e1d02bfa8f5f81ab95a734aba211ceb".Sha256())
                    },
                    RedirectUris = new List<string>
                    {
                        "http://localhost:60001/signin-oidc",
                        "http://localhost:26007/signin-oidc",
                        "https://api.meyer-connect.com/signin-oidc",
                        "https://wiki.meyer-connect.com/oauthcallback",
                        "http://localhost:4200/auth-callback",
                        "http://localhost:4200",
                        "http://localhost:4300",
                        "https://ltes.meyer-connect.com",
                        "https://www.meyer-connect.com/auth-callback",
                        "http://127.0.0.1:8080"
                    },
                    PostLogoutRedirectUris = new List<string>
                    {
                        "http://localhost:60001/signout-callback-oidc",
                        "http://localhost:26007/signout-callback-oidc",
                        "https://api.meyer-connect.com/signout-callback-oidc",
                        "https://wiki.meyer-connect.com/signout-callback-oidc",
                        "http://localhost:4200",
                        "http://localhost:4300",
                        "https://www.meyer-connect.com/",
                        "https://ltes.meyer-connect.com",
                        "http://127.0.0.1:8080"
                    },
                    AllowedCorsOrigins = new List<string>
                    {
                        "http://localhost:60001",
                        "http://localhost:26007",
                        "https://api.meyer-connect.com",
                        "https://www.meyer-connect.com",
                        "https://wiki.meyer-connect.com",
                        "http://localhost:4200",
                        "http://localhost:4300",
                        "https://ltes.meyer-connect.com",
                        "http://127.0.0.1:8080"
                    },
                    AllowedScopes = new List<string>
                    {
                        StandardScopes.OpenId,
                        StandardScopes.Profile,
                        StandardScopes.OfflineAccess,
                        "api1",
                        "mcPermissionRoles",
                        StandardScopes.Email,
                        "dataeventrecords",
                        "meyerConnectRoles",
                        "role",
                    },
                    AccessTokenLifetime = 3600,
                    AllowOfflineAccess = true,
                    AlwaysIncludeUserClaimsInIdToken = true
                }
            };
        }
    }
}