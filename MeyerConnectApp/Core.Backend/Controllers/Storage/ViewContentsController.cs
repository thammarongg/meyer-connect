﻿using Core.Backend.Repositories.Interface;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.IO;

namespace Core.Backend.Controllers.Storage
{
    [Route("api/storage/uc")]
    [ApiController]
    public class ViewContentsController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly IUrlHelper _urlHelper;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IStorageRepository _storageRepository;

        public ViewContentsController(
            ILogger<ViewContentsController> logger,
            IUrlHelper urlHelper,
            IHostingEnvironment hostingEnvironment,
            IStorageRepository storageRepository
            )
        {
            _logger = logger;
            _urlHelper = urlHelper;
            _hostingEnvironment = hostingEnvironment;
            _storageRepository = storageRepository;
        }

        [HttpGet(Name = "GetFileContentById")]
        public IActionResult GetFileContentById(string id, string type = "view")
        {
            try
            {
                string folderName = "Storages\\Data";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string storagePath = Path.Combine(webRootPath, folderName);

                if (!Directory.Exists(storagePath))
                {
                    throw new DirectoryNotFoundException("directory not found");
                }

                var storageFile = _storageRepository.GetFileById(id);
                if (storageFile != null)
                {
                    string fullPath = Path.Combine(
                            storagePath,
                            storageFile.FileTitle
                            );

                    switch (type)
                    {
                        case "view":
                            return PhysicalFile(fullPath, storageFile.MimeType);

                        case "download":
                            return PhysicalFile(fullPath, storageFile.MimeType, storageFile.FileTitle);

                        default:
                            return PhysicalFile(fullPath, storageFile.MimeType);
                    }
                }
                else
                {
                    return NotFound("file not found");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}