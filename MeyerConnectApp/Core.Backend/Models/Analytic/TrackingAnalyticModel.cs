﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Models.Analytic
{
    public class TrackingAnalyticModel
    {
        public string pagePath { get; set; }
        public string pageTitle { get; set; }
        public int userId { get; set; }
        public DateTime activeDate { get; set; }
        public int hour { get; set; }
        public int minute { get; set; }
        public double timeOnPage { get; set; }

        //optional
        public string fullName { get; set; }
        public string email { get; set; }
        public string Id { get; set; }
    }
}
