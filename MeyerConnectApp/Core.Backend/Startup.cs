﻿using Core.Backend.Hubs;
using Core.Backend.Models;
using Core.Backend.Repositories;
using Core.Backend.Repositories.Interface;
using Core.Backend.Services;
using Core.Backend.Services.Subscription;
using Hangfire;
using Hangfire.Dashboard;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;

namespace Core.Backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            // ASP.Net core identity
            var connectionString = @"data source=MILDB;initial catalog=MeyerConnectApp;user id=sa;password=sql@Meyer";
            services.AddDbContext<AppDbContext>(options => options.UseSqlServer(connectionString));

            // Hangfire
            services.AddHangfire(c => c.UseSqlServerStorage(connectionString));

            services.AddMvcCore()
                .AddJsonFormatters()
                .AddJsonOptions(options =>
                {
                    // handler loops correctly
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

                    // use stadard name conversion of properties
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

                    // include $id property in the output
                    options.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;
                })
                .AddAuthorization();

            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(1, 0);
            });

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddIdentityServerAuthentication(options =>
            {
                // base-address of your identityserver
                options.Authority = Environment.IsDevelopment() ? "http://localhost:60000" : "https://secure.meyer-connect.com";
                options.RequireHttpsMetadata = false;

                // name of the API resource
                options.ApiName = "mcPermissionRoles";
            });

            services.AddCors(options =>
            {
                // this defines a CORS policy called "default"
                options.AddPolicy("defaultAllAlow", policy =>
                {
                    policy.WithOrigins(new string[] {
                        "http://localhost:60000",
                        "https://www.meyer-connect.com",
                        "http://localhost:4200",
                        "https://ltes.meyer-connect.com",
                        "http://localhost:4300"
                    })
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });

            services.AddSignalR();

            services.AddAuthorization(options =>
            {
                options.AddPolicy("sysadmin", policy => policy.RequireRole("sysadmin"));
                options.AddPolicy("staff", policy => policy.RequireRole("staff"));
            });

            services.AddSqlServerDbContextFactory<AppSQLTableDependencyContext>(connectionString);
            services.AddSingleton<PostDatabaseSubscription, PostDatabaseSubscription>();
            //services.AddScoped<IHubContext<PostHub>, HubContext<PostHub>>();

            services.AddSingleton<IEmailSender, EmailSender>();
            services.AddSingleton<IStaffsRepository, StaffsRepository>();
            services.AddScoped<IPostRepository, PostRepository>();
            services.AddScoped<IStorageRepository, StorageRepository>();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddSingleton<IGoogleAnalyticService, GoogleAnalyticService>();
            services.AddScoped<IUrlHelper>(factory =>
            {
                var actionContext = factory.GetService<IActionContextAccessor>()
                                           .ActionContext;
                return new Microsoft.AspNetCore.Mvc.Routing.UrlHelper(actionContext);
            });
            services.Configure<EmailSenderOptions>(Configuration.GetSection("EmailClientSettings"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("defaultAllAlow");

            app.UseAuthentication();

            app.UseSignalR(routes =>
            {
                routes.MapHub<PostHub>("/PostHub");
            });

            app.UseMvcWithDefaultRoute();

            app.UseStaticFiles();

            app.UseHangfireServer();

            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                
                
            });

            //app.UseSqlTableDependency<PostDatabaseSubscription>(@"data source=MILDB;initial catalog=MeyerConnectApp;user id=sa;password=sql@Meyer");
        }
    }
}