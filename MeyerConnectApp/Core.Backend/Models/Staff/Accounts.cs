﻿using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Core.Backend.Models.Staff
{
    public class Accounts : IdentityUser
    {
        

        // 1 = new register; 2 = active, 3 = disable, 4 = suspend
        [Required]
        public int AccountStatus { get; set; }

        [Required]
        public string CreatedAccountBy { get; set; }

        [Required]
        public DateTime CreatedAccountDateUtc { get; set; }

        [Required]
        public string FullName { get; set; }

        public DateTime LastLoginUtc { get; set; }
        public string AccountProfileImageUrl { get; set; }

        [Required]
        public string DefaultRole { get; set; }

        public virtual AccountProfile AccountProfile { get; set; }

        public virtual ICollection<AccountPhoneContact> AccountPhoneContacts { get; set; }


        #region overrides

        public override string Email { get; set; }

        [JsonIgnore]
        public override bool EmailConfirmed { get; set; }

        [JsonIgnore]
        public override bool TwoFactorEnabled { get; set; }

        [JsonIgnore]
        public override string PhoneNumber { get; set; }

        [JsonIgnore]
        public override bool PhoneNumberConfirmed { get; set; }

        [JsonIgnore]
        public override string PasswordHash { get; set; }

        [JsonIgnore]
        public override string SecurityStamp { get; set; }

        [JsonIgnore]
        public override bool LockoutEnabled { get; set; }

        [JsonIgnore]
        public override int AccessFailedCount { get; set; }

        [JsonIgnore]
        public override string NormalizedUserName { get; set; }

        [JsonIgnore]
        public override string NormalizedEmail { get; set; }

        [JsonIgnore]
        public override string ConcurrencyStamp { get; set; }

        #endregion
    }
}