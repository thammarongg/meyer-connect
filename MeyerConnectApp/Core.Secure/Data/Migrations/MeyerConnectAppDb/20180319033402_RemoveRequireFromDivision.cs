﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Core.Secure.Data.Migrations.MeyerConnectAppDb
{
    public partial class RemoveRequireFromDivision : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Division",
                table: "AccountProfile",
                nullable: true,
                oldClrType: typeof(string));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Division",
                table: "AccountProfile",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
