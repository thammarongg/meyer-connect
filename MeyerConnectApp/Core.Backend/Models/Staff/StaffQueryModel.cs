﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Models.Staff
{
    public class StaffQueryModel
    {
        public string Colleague { get; set; } = null;
        public int MaxResult { get; set; } = 150;

        public string Company { get; set; } = null;
        public string Division { get; set; } = null;
        public string Department { get; set; } = null;
    }
}
