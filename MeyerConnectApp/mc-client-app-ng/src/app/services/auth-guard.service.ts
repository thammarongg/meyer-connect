// import { Observable } from 'rxjs/Observable';
// import {
//   CanActivate,
//   ActivatedRouteSnapshot,
//   RouterStateSnapshot
// } from '@angular/router';
// import { Injectable } from '@angular/core';
// import { AuthService } from './auth.service';

// @Injectable()
// export class AuthGuardService implements CanActivate {
//   canActivate(
//     route: ActivatedRouteSnapshot,
//     state: RouterStateSnapshot
//   ): boolean | Observable<boolean> | Promise<boolean> {
//     if (this.authService.isLoggedIn()) {
//       return true;
//     }

//     this.authService.startAuthentication();
//     return false;
//   }
//   constructor(private authService: AuthService) {}
// }

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  CanActivateChild,
  CanLoad,
  NavigationExtras,
  Route
} from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuardService
  implements CanActivate, CanActivateChild, CanLoad {
  constructor(private oauthService: OAuthService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {
    const url: string = state.url;
    return this.checkLogin(url);
  }

  canLoad(route: Route): boolean | Observable<boolean> | Promise<boolean> {
    const url = `/${route.path}`;
    return this.checkLogin(url);
  }
  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {
    return this.canActivate(childRoute, state);
  }

  checkLogin(url: string): boolean {
    if (this.oauthService.hasValidAccessToken()) {
      return true;
    }

    this.router.navigate(['request-auth']);
    return false;
  }
}
