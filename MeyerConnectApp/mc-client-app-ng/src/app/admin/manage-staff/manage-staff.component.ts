import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { StaffService } from '../../services/staff.service';
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-manage-staff',
  templateUrl: './manage-staff.component.html',
  styleUrls: ['./manage-staff.component.css']
})
export class ManageStaffComponent implements OnInit, OnDestroy {
  staffs: any[];
  staff: any;
  constructor(private staffService: StaffService) {}

  ngOnInit() {
    this.getStaffs();
  }

  getStaffs(): void {
    this.staffService.getStaffs().subscribe((staffs: any) => {
      this.staffs = staffs.items;
      $(document).ready(function() {
        $('.staff-action').dropdown();
        $('.is-popup').popup();
      });
    });
  }

  onSorted($event) {
    this.getStaffs();
  }

  findStaffsByColleague(colleague: string): void {
    this.staffs = [];
    this.staffService
      .getStaffsByColleague(colleague)
      .subscribe((staffs: any) => {
        this.staffs = staffs.items;
        $(document).ready(function() {
          $('.staff-action').dropdown();
          $('.is-popup').popup();
        });
      });
  }

  sendInvitation(id: string): void {
    console.log(id);
    this.staffService.sendInvitation(id).subscribe((data: any) => {});
  }

  disableAccount(id: string): void {
    console.log(id);
  }

  editStaffInfo(): void {}

  deactivatedStaff(): void {}

  showInfoModal(id: string): void {
    if (id) {
      this.staffService.getStaffById(id).subscribe((staff: any) => {
        this.staff = staff;
      });
    }
  }

  ngOnDestroy(): void {}
}
