﻿using Core.Backend.Models;
using Core.Backend.Repositories;
using Core.Backend.Repositories.Interface;
using Core.Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Backend.Controllers.Staff
{
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/staff/v{version:apiVersion}/staffs")]
    //[Authorize]
    public class StaffsController : Controller
    {
        private readonly AppDbContext _context;
        private readonly ILogger _logger;
        private readonly IEmailSender _emailSender;
        private readonly IStaffsRepository _repo;
        private readonly IAuthorizationService _authorization;

        public StaffsController(
            AppDbContext context,
            ILogger<StaffsController> logger,
            IEmailSender emailSender,
            IStaffsRepository repo,
            IAuthorizationService authorization)
        {
            _context = context;
            _logger = logger;
            _emailSender = emailSender;
            _repo = repo;
            _authorization = authorization;
        }

        // GET: api/staff/{ApiVersion}/staffs
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Get()
        {
            dynamic jsonResult = new JObject();

            try
            {
                //var allowed = await _authorization.AuthorizeAsync(User, "sysadmin");
                //if (allowed.Succeeded)
                //{
                //    return Ok("test");
                //}

                var items = await _repo.GetAll(int.MaxValue);
                if (!items.Any())
                {
                    _logger.LogWarning("warn-03-001: not found data.");
                    return NotFound("not found data.");
                }

                _logger.LogInformation("return json data");
                jsonResult.items = JToken.FromObject(items);
                return Ok(items);
            }
            catch (Exception ex)
            {
                _logger.LogError("err-03-001: something wrong.", new object[] { ex.Message });
                jsonResult.message = ex.Message;
                return StatusCode(500, jsonResult);
            }
        }

        // GET: api/{ApiVersion}/staff/staffs/{userId}
        [HttpGet("{id}", Name = "GetStaffById")]
        public async Task<IActionResult> GetStaffById(string id)
        {
            dynamic jsonResult = new JObject();

            try
            {
                var item = await _repo.GetById(id);
                if (item == null)
                {
                    _logger.LogWarning("warn-03-001: not found data.");
                    return NotFound("not found data.");
                }

                _logger.LogInformation("return json data");

                return Ok(item);
            }
            catch (Exception ex)
            {
                _logger.LogError("err-03-001: something wrong.", new object[] { ex.Message });
                jsonResult.message = ex.Message;
                return StatusCode(500, jsonResult);
            }
        }

        [HttpGet("isAdminAccount")]
        [Authorize(Policy = "sysadmin")]
        public IActionResult IsAdminAccount()
        {
            return Ok(true);
        }

        // POST: api/Staffs
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Staffs/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}