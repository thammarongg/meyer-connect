﻿using Core.Backend.Models.Post;
using Microsoft.EntityFrameworkCore;

namespace Core.Backend.Models
{
    public class AppSQLTableDependencyContext : DbContext
    {
        public AppSQLTableDependencyContext(DbContextOptions<AppSQLTableDependencyContext> options)
           : base(options)
        {
        }

        public DbSet<Posts> Posts { get; set; }
        public DbSet<PostAudiences> PostAudiences { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}