import { Component, OnInit } from '@angular/core';
import { Weather } from './weather';
import { WeatherService } from './weather.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {
  weather: Weather;
  constructor(private weatherService: WeatherService) {
    this.weather = new Weather();
  }

  ngOnInit() {
    this.getWeatherByLocation();
  }

  getWeatherByLocation(): void {
    this.weatherService.getWeatherByLocation('Hongkong,HK').subscribe(data => {
      const result = data.query.results.channel;
      this.weather = {
        temperature: result.item.condition.temp,
        conditionCode: result.item.condition.code,
        conditionText: result.item.condition.text.toLowerCase(),
        unit: result.units.temperature,
        city: result.location.city,
        country: result.location.country,
        region: result.location.region
      };
    });
  }
}
