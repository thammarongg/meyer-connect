import { environment } from './../environments/environment';
import { CalendarComponent } from './pages/calendar/calendar.component';
import { PageLayoutComponent } from './pages/page-layout/page-layout.component';
import { AdminComponent } from './admin/admin/admin.component';
import { CanDeactivateGuard } from './services/can-deactivate-guard.service';
import { HelpComponent } from './pages/help/help.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { StaffDirectoriesComponent } from './pages/staff-directories/staff-directories.component';
import { NewsAnnouncementComponent } from './pages/news-announcement/news-announcement.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { AuthGuardService } from './services/auth-guard.service';
import { AuthCallbackComponent } from './auth-callback/auth-callback.component';
import { RequestAuthComponent } from './request-auth/request-auth.component';
import { SelectivePreloadingStrategy } from './selective-preloading-strategy';
import { ManageStaffComponent } from './admin/manage-staff/manage-staff.component';
import { ManageAnnouncementComponent } from './admin/manage-announcement/manage-announcement.component';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'news-announcement',
    pathMatch: 'full'
  },
  {
    path: '',
    component: PageLayoutComponent,
    canLoad: [AuthGuardService],
    children: [
      {
        path: 'news-announcement',
        component: NewsAnnouncementComponent,
        canActivate: [AuthGuardService]
      },
      {
        path: 'staff-directories',
        component: StaffDirectoriesComponent,
        canActivate: [AuthGuardService]
      },
      {
        path: 'help',
        component: HelpComponent,
        canActivate: [AuthGuardService]
      },
      {
        path: 'calendar',
        component: CalendarComponent,
        canActivate: [AuthGuardService]
      }
    ]
  },
  {
    path: 'admin',
    component: AdminComponent,
    canLoad: [AuthGuardService],
    children: [
      {
        path: '',
        children: [
          {
            path: 'staff',
            component: ManageStaffComponent,
            canActivate: [AuthGuardService]
          },
          {
            path: 'announcement',
            component: ManageAnnouncementComponent,
            canActivate: [AuthGuardService]
          },
          {
            path: '',
            component: AdminDashboardComponent,
            canActivate: [AuthGuardService]
          }
        ]
      }
    ]
  },
  {
    path: 'auth-callback',
    component: AuthCallbackComponent,
    redirectTo: ''
  },
  {
    path: 'request-auth',
    component: RequestAuthComponent
  },
  { path: 'signout-callback-oidc', redirectTo: '' },
  { path: '**', component: NotfoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {
      enableTracing: environment.enableTracing, // <-- debugging purposes only
      preloadingStrategy: SelectivePreloadingStrategy
    })
  ],
  exports: [RouterModule],
  providers: [CanDeactivateGuard, SelectivePreloadingStrategy]
})
export class AppRoutingModule {}
