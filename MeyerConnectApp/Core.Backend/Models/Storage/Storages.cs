﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Backend.Models.Storage
{
    public class Storages
    {
        [Key]
        public string FileId { get; set; }

        [Required]
        public string FileTitle { get; set; }

        public string Description { get; set; }

        [Required]
        public string ContentLinkUrl { get; set; }

        [Required]
        public string MimeType { get; set; }

        [Required]
        public DateTime ModifiedDateUtc { get; set; } = DateTime.UtcNow;

        [Required]
        public string CreatedBy { get; set; }

        [Required]
        public bool Trashed { get; set; }

        [Required]
        public string Visibility { get; set; } // anyoneCanFind, anyoneWithLink, limited
    }
}