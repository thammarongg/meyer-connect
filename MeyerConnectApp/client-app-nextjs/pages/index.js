import React from "react"
import { Modal, Header, Button, List } from "semantic-ui-react"
import Layout from "../components/layout"
import stylesheet from "../styles/index.scss"
import { initStore } from "../store"
import { Provider } from "mobx-react"

export default class Index extends React.Component {
  static getInitialProps({ req }) {
    const isServer = !!req
    const store = initStore(isServer)
    return { lastUpdate: store.lastUpdate, isServer }
  }

  constructor(props) {
    super(props)
    this.store = initStore(props.isServer, props.lastUpdate)
  }

  render() {
    return (
      <Provider store={this.store}>
        <Layout title="Meyer Connect App">
          <style dangerouslySetInnerHTML={{ __html: stylesheet }} />
          <p>ciao</p>
        </Layout>
      </Provider>
    )
  }
}
