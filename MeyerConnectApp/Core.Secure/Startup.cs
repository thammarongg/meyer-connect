﻿using Core.Secure.Configuration;
using Core.Secure.Models;
using Core.Secure.Services;
using IdentityModel;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;
using System;
using System.IO;
using System.Reflection;

namespace Core.Secure
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            // ASP.Net core identity
            var connectionString = @"data source=MILDB;initial catalog=MeyerConnectApp;user id=sa;password=sql@Meyer";

            // add user context
            services.AddDbContext<AuthDbContext>(options =>
                options.UseSqlServer(connectionString,
                    sql =>
                        sql.MigrationsAssembly(
                            typeof(Startup).GetTypeInfo().Assembly.GetName().Name)));

            services.AddIdentity<Accounts, IdentityRole>(options =>
            {
                // password settings
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 6;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireLowercase = true;
                options.Password.RequireUppercase = true;

                // user settings
                options.User.RequireUniqueEmail = true;

                // lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;
                options.Lockout.AllowedForNewUsers = true;
            })
            .AddEntityFrameworkStores<AuthDbContext>()
            .AddDefaultTokenProviders();
            //.AddClaimsPrincipalFactory<CustomUserClaimsPrincipalFactory>();

            services.AddScoped<IUserClaimsPrincipalFactory<Accounts>, CustomUserClaimsPrincipalFactory>();

            // Add application services.
            //services.AddTransient<IProfileService, ClaimsProfileService>();
            services.AddTransient<IEmailSender, EmailSender>();
            services.Configure<EmailSenderOptions>(Configuration.GetSection("EmailClientSettings"));
            services.AddSingleton<IFileProvider>(new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")));

            // add identityserver
            var certificateSettings = Configuration.GetSection("CertificateSettings");
            //var cert = new X509Certificate2(
            //    certificateSettings.GetValue<string>("FileName"),
            //    certificateSettings.GetValue<string>("Password"));
            //var cert = new X509Certificate2(Path.Combine(Environment.WebRootPath, "public_mc.pfx"), "Meyer-mil.com99");
            services.AddIdentityServer(options =>
            {
                options.Events.RaiseErrorEvents = true;
                options.Events.RaiseInformationEvents = true;
                options.Events.RaiseFailureEvents = true;
                options.Events.RaiseSuccessEvents = true;
            })
                .AddDeveloperSigningCredential()
                //.AddProfileService<ClaimsProfileService>()
                //.AddSigningCredential(cert)
                .AddSecretValidator<PrivateKeyJwtSecretValidator>()
                // in-memory
                .AddInMemoryIdentityResources(ClientStore.GetIdentityResource())
                .AddInMemoryApiResources(ClientStore.GetApiResources())
                .AddInMemoryClients(ClientStore.GetClients())
                // add config data from db
                .AddAspNetIdentity<Accounts>();

            services.AddCors(options =>
            {
                // this defines a CORS policy called "default"
                options.AddPolicy("default", policy =>
                {
                    policy.WithOrigins(new string[] {
                        "http://localhost:60000",
                        "https://www.meyer-connect.com",
                        "http://localhost:4200",
                        "http://localhost:4300",
                        "https://ltes.meyer-connect.com",
                    })
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });

            // add mvc
            services.AddMvc()
                .AddJsonOptions(options =>
                {
                    // handler loops correctly
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

                    // use stadard name conversion of properties
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

                    // include $id property in the output
                    options.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;
                });

            services.AddAuthentication()
                .AddOpenIdConnect("oidc", "OpenID Connect", options =>
                {
                    options.Authority = Environment.IsDevelopment() ? "http://localhost:60000" : "https://secure.meyer-connect.com";
                    options.ClientId = "implicit";
                    options.SaveTokens = true;
                    options.RequireHttpsMetadata = false;
                    options.GetClaimsFromUserInfoEndpoint = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        NameClaimType = JwtClaimTypes.Name,
                        RoleClaimType = JwtClaimTypes.Role
                    };
                });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("sysadmin", policy => policy.RequireRole("sysadmin"));
                options.AddPolicy("staff", policy => policy.RequireRole("staff"));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseCors("default");

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseIdentityServer();

            app.UseMvcWithDefaultRoute();
        }

        //// Initialize some test roles. In the real world, these would be setup explicitly by a role manager
        //private string[] roles = new[] { "User", "Manager", "Administrator" };
        //private async Task InitializeRoles(RoleManager<IdentityRole> roleManager)
        //{
        //    foreach (var role in roles)
        //    {
        //        if (!await roleManager.RoleExistsAsync(role))
        //        {
        //            var newRole = new IdentityRole(role);
        //            await roleManager.CreateAsync(newRole);
        //            // In the real world, there might be claims associated with roles
        //            // await roleManager.AddClaimAsync(newRole, new Claim("foo", "bar"))
        //        }
        //    }
        //}
    }
}