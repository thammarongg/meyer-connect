import { createUserManager } from "redux-oidc"

const config = {
  authority: "http://localhost:60000",
  client_id: "meyerconnectapp",
  redirect_uri: `${window.location.protocol}//${window.location.hostname}${
    window.location.port ? `:${window.location.port}` : ""
  }/callback`,
  response_type: "token id_token",
  scope: "openid profile offline_access api1 mcPermissionRoles mcPermissionScopes email role",
  loadUserInfo: true,
  automaticSilentRenew: true,
  filterProtocolClaims: true
}

const userManager = createUserManager(config)

export default userManager
